--CREATE DATABASE cultural_routes_production WITH ENCODING = 'UTF8'

CREATE TABLE authors (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	birthdate INTEGER,
	deathdate INTEGER,
	uncertain_birthdate BOOLEAN DEFAULT FALSE,
	uncertain_deathdate BOOLEAN DEFAULT FALSE,
	biography TEXT,
	created_at TIMESTAMP,
	updated_at TIMESTAMP
);

CREATE TABLE themes (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(64) NOT NULL,
	description TEXT,
	"from" INTEGER,
	"to" INTEGER,
	created_at TIMESTAMP,
	updated_at TIMESTAMP
);


CREATE TABLE literary_routes (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	description TEXT NOT NULL,
	author_id BIGINT,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
	
	FOREIGN KEY (author_id) REFERENCES authors(id) ON DELETE CASCADE
);

CREATE INDEX idx_literary_route_author_id ON literary_routes(author_id);

CREATE TABLE literary_routes_themes (
	literary_route_id BIGINT NOT NULL,
	theme_id BIGINT NOT NULL,
	PRIMARY KEY (literary_route_id, theme_id),
	FOREIGN KEY (literary_route_id) REFERENCES literary_routes(id) ON DELETE CASCADE,
	FOREIGN KEY (theme_id) REFERENCES themes(id) ON DELETE CASCADE
);

CREATE INDEX idx_lrt_literary_route_id ON literary_routes_themes(literary_route_id);
CREATE INDEX idx_lrt_theme_id ON literary_routes_themes(theme_id);

CREATE TABLE locations (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	description	TEXT,
	location VARCHAR(512) NOT NULL,
	latitude numeric(10,6),
	longitude numeric(10,6),
  weight SMALLINT DEFAULT 0,
	literary_route_id BIGINT NOT NULL,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
	
	FOREIGN KEY (literary_route_id) REFERENCES literary_routes(id) ON DELETE CASCADE
);

CREATE INDEX idx_locations_literary_route_id ON locations(literary_route_id);

CREATE TABLE books (
	id BIGSERIAL PRIMARY KEY,
	title VARCHAR(256) NOT NULL,
	"date" INTEGER,
	location VARCHAR(256) NOT NULL,
	publisher VARCHAR(128)  NOT NULL,
	created_at TIMESTAMP,
	updated_at TIMESTAMP
);

CREATE TABLE authors_books (
	book_id BIGINT NOT NULL,
	author_id BIGINT NOT NULL,
	PRIMARY KEY (author_id, book_id),
	FOREIGN KEY (book_id) REFERENCES books(id) ON DELETE CASCADE,
	FOREIGN KEY (author_id) REFERENCES authors(id) ON DELETE CASCADE
);

CREATE INDEX idx_books_author_id ON books(author_id);

CREATE TABLE citations (
	id BIGSERIAL PRIMARY KEY,
	citation TEXT NOT NULL,
	pages VARCHAR(24),
	location_id BIGINT NOT NULL,
	book_id BIGINT NOT NULL,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
	
	FOREIGN KEY (location_id) REFERENCES locations(id) ON DELETE CASCADE,
	FOREIGN KEY (book_id) REFERENCES books(id) ON DELETE CASCADE
);

CREATE INDEX idx_citations_location_id ON citations(location_id);
CREATE INDEX idx_citations_book_id ON citations(book_id);


-- statues
CREATE TABLE artists (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	birthdate INTEGER,
	deathdate INTEGER,
	uncertain_birthdate BOOLEAN DEFAULT FALSE,
	uncertain_deathdate BOOLEAN DEFAULT FALSE,
	biography TEXT NOT NULL,
	created_at TIMESTAMP,
	updated_at TIMESTAMP
);

CREATE TABLE related_works (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	description TEXT,
	"date" INTEGER,
	artist_id BIGINT NOT NULL,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
	
	FOREIGN KEY (artist_id) REFERENCES artists(id) ON DELETE CASCADE
);

CREATE INDEX idx_related_works_artist_id ON related_works(artist_id);

CREATE TABLE entities (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	description TEXT,
	birthdate INTEGER,
	deathdate INTEGER,
	uncertain_birthdate BOOLEAN DEFAULT FALSE,
	uncertain_deathdate BOOLEAN DEFAULT FALSE,
	created_at TIMESTAMP,
	updated_at TIMESTAMP
);

CREATE TABLE categories (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	description TEXT,
	created_at TIMESTAMP,
	updated_at TIMESTAMP
);

CREATE TABLE statues (
	id BIGSERIAL PRIMARY KEY,
	title VARCHAR(256) NOT NULL,
	description TEXT NOT NULL,
	location VARCHAR(512),
	comment TEXT,
	latitude numeric(10, 6),
	longitude numeric(10, 6),
	"date" INTEGER,
	artist_id BIGINT NOT NULL,
	entity_id BIGINT,
  author_id BIGINT, 
	category_id BIGINT NOT NULL,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,

  FOREIGN KEY (artist_id) REFERENCES artists(id),
  FOREIGN KEY (entity_id) REFERENCES entities(id),
  FOREIGN KEY (category_id) REFERENCES categories(id),
  FOREIGN KEY (author_id) REFERENCES authors(id)
);

CREATE INDEX idx_statues_artist_id ON statues(artist_id);
CREATE INDEX idx_statues_entity_id ON statues(entity_id);
CREATE INDEX idx_statues_category_id ON statues(category_id);

CREATE TABLE assets (
	id	 BIGSERIAL PRIMARY KEY,
	title VARCHAR(256) NOT NULL,
	description TEXT,
	asset_file_name VARCHAR(128),
	asset_content_type VARCHAR(64),
	asset_file_size INT,
	asset_updated_at TIMESTAMP,
	image_url VARCHAR(1024),
	video_url VARCHAR(1024),
	embed_code TEXT,
	media_type VARCHAR(24),
	assetable_id BIGINT NOT NULL,
	assetable_type VARCHAR(32) NOT NULL,
	width INT,
	height INT,
	created_at TIMESTAMP,
	updated_at TIMESTAMP
	
	--FOREIGN KEY (assetable_id) REFERENCES assetable(id) ON DELETE CASCADE
);

CREATE INDEX idx_assets_id ON assets(assetable_id);


CREATE TABLE pages (
  id  BIGSERIAL PRIMARY KEY,
  link VARCHAR(32) NOT NULL,
  name VARCHAR(256) NOT NULL
);

CREATE TABLE blocks (
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR(256) NOT NULL,
  content TEXT NOT NULL,
  page_id BIGINT NOT NULL,
  weight smallint default 0,
  FOREIGN KEY (page_id) REFERENCES pages (id)

);

CREATE INDEX idx_page_id ON blocks(page_id);

INSERT INTO pages (id, name) VALUES (1, 'Home');
INSERT INTO pages (id, name) VALUES (2, 'Sobre');
INSERT INTO pages (id, name) VALUES (3, 'Contactos');

INSERT INTO blocks (title, content, weight, page_id) VALUES ('Percursos Literários', '', 0, 1);
INSERT INTO blocks (title, content, weight, page_id) VALUES ('Geo-Monumentos', '', 1, 1);
INSERT INTO blocks (title, content, weight, page_id) VALUES ('Quem Somos', '', 0, 2);
INSERT INTO blocks (title, content, weight, page_id) VALUES ('Motivação', '', 1, 2);
INSERT INTO blocks (title, content, weight, page_id) VALUES ('Contactos', '', 0, 3);

-- Functions
CREATE OR REPLACE FUNCTION distance (Lat1 IN NUMERIC,
                                     Lon1 IN NUMERIC,
                                     Lat2 IN NUMERIC,
                                     Lon2 IN NUMERIC,
                                     Radius IN NUMERIC DEFAULT 3963) RETURNS NUMERIC AS $$
 -- Convert degrees to radians
DECLARE DegToRad NUMERIC = 57.29577951;
BEGIN
  RETURN (COALESCE(Radius,0) * ACOS((sin(COALESCE(Lat1,0) / DegToRad) * SIN(COALESCE(Lat2,0) / DegToRad)) +
        (COS(COALESCE(Lat1,0) / DegToRad) * COS(COALESCE(Lat2,0) / DegToRad) *
         COS(COALESCE(Lon2,0) / DegToRad - COALESCE(Lon1,0)/ DegToRad))));
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION initials() RETURNS SETOF char AS $$
DECLARE r char;
DECLARE sql TEXT = 'SELECT DISTINCT UPPER(SUBSTRING(name FROM 1 FOR 1))  AS letter FROM entities 
    JOIN statues ON statues.entity_id = entities.id
    UNION
    SELECT DISTINCT UPPER(SUBSTRING(name FROM 1 FOR 1))  AS letter FROM authors 
    JOIN statues ON statues.author_id = authors.id 
    ORDER BY letter';

BEGIN
  FOR r in EXECUTE sql LOOP 
    RETURN NEXT r;
  END LOOP;
  RETURN;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION initials_for(_table text, _field text) RETURNS SETOF char AS $$
DECLARE r char;
DECLARE sql TEXT = 'SELECT DISTINCT UPPER(SUBSTRING(' || _field || ' FROM 1 FOR 1))  AS letter FROM ' || _table || ' ORDER BY letter';


BEGIN
  FOR r in EXECUTE sql LOOP 
    RETURN NEXT r;
  END LOOP;
  RETURN;
END;
$$ LANGUAGE PLPGSQL;
