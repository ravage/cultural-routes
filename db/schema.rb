# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20100608033219) do

  create_table "artists", :force => true do |t|
    t.string   "name",                :limit => 256,                    :null => false
    t.integer  "birthdate"
    t.integer  "deathdate"
    t.boolean  "uncertain_birthdate",                :default => false
    t.boolean  "uncertain_deathdate",                :default => false
    t.text     "biography",                                             :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assets", :force => true do |t|
    t.string   "title",              :limit => 256,  :null => false
    t.text     "description"
    t.string   "asset_file_name",    :limit => 128
    t.string   "asset_content_type", :limit => 64
    t.integer  "asset_file_size"
    t.datetime "asset_updated_at"
    t.string   "image_url",          :limit => 1024
    t.string   "video_url",          :limit => 1024
    t.text     "embed_code"
    t.string   "media_type",         :limit => 24
    t.integer  "assetable_id",       :limit => 8,    :null => false
    t.string   "assetable_type",     :limit => 32,   :null => false
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "assets", ["assetable_id"], :name => "idx_assets_id"

  create_table "authors", :force => true do |t|
    t.string   "name",                :limit => 256,                    :null => false
    t.integer  "birthdate"
    t.integer  "deathdate"
    t.boolean  "uncertain_birthdate",                :default => false
    t.boolean  "uncertain_deathdate",                :default => false
    t.text     "biography"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authors_books", :id => false, :force => true do |t|
    t.integer "book_id",   :limit => 8, :null => false
    t.integer "author_id", :limit => 8, :null => false
  end

  create_table "blocks", :force => true do |t|
    t.string  "title",   :limit => 256,                :null => false
    t.text    "content",                               :null => false
    t.integer "page_id", :limit => 8,                  :null => false
    t.integer "weight",  :limit => 2,   :default => 0
  end

  add_index "blocks", ["page_id"], :name => "idx_page_id"

  create_table "books", :force => true do |t|
    t.string   "title",      :limit => 256, :null => false
    t.integer  "date"
    t.string   "location",   :limit => 256, :null => false
    t.string   "publisher",  :limit => 128, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", :force => true do |t|
    t.string   "name",        :limit => 256, :null => false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "citations", :force => true do |t|
    t.text     "citation",                  :null => false
    t.string   "pages",       :limit => 24
    t.integer  "location_id", :limit => 8,  :null => false
    t.integer  "book_id",     :limit => 8,  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "citations", ["book_id"], :name => "idx_citations_book_id"
  add_index "citations", ["location_id"], :name => "idx_citations_location_id"

  create_table "entities", :force => true do |t|
    t.string   "name",                :limit => 256,                    :null => false
    t.text     "description"
    t.integer  "birthdate"
    t.integer  "deathdate"
    t.boolean  "uncertain_birthdate",                :default => false
    t.boolean  "uncertain_deathdate",                :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "literary_routes", :force => true do |t|
    t.string   "name",        :limit => 256, :null => false
    t.text     "description",                :null => false
    t.integer  "author_id",   :limit => 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "literary_routes", ["author_id"], :name => "idx_literary_route_author_id"

  create_table "literary_routes_themes", :id => false, :force => true do |t|
    t.integer "literary_route_id", :limit => 8, :null => false
    t.integer "theme_id",          :limit => 8, :null => false
  end

  add_index "literary_routes_themes", ["literary_route_id"], :name => "idx_lrt_literary_route_id"
  add_index "literary_routes_themes", ["theme_id"], :name => "idx_lrt_theme_id"

  create_table "locations", :force => true do |t|
    t.string   "name",              :limit => 256,                                               :null => false
    t.text     "description"
    t.string   "location",          :limit => 512,                                               :null => false
    t.decimal  "latitude",                         :precision => 10, :scale => 6
    t.decimal  "longitude",                        :precision => 10, :scale => 6
    t.integer  "weight",            :limit => 2,                                  :default => 0
    t.integer  "literary_route_id", :limit => 8,                                                 :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations", ["literary_route_id"], :name => "idx_locations_literary_route_id"

  create_table "pages", :force => true do |t|
    t.string "name", :limit => 256, :null => false
  end

  create_table "related_works", :force => true do |t|
    t.string   "name",        :limit => 256, :null => false
    t.text     "description"
    t.integer  "date"
    t.integer  "artist_id",   :limit => 8,   :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "related_works", ["artist_id"], :name => "idx_related_works_artist_id"

  create_table "sessions", :force => true do |t|
    t.text     "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "statues", :force => true do |t|
    t.string   "title",       :limit => 256,                                :null => false
    t.text     "description",                                               :null => false
    t.string   "location",    :limit => 512
    t.text     "comment"
    t.decimal  "latitude",                   :precision => 10, :scale => 6
    t.decimal  "longitude",                  :precision => 10, :scale => 6
    t.integer  "date"
    t.integer  "artist_id",   :limit => 8,                                  :null => false
    t.integer  "entity_id",   :limit => 8
    t.integer  "author_id",   :limit => 8
    t.integer  "category_id", :limit => 8,                                  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "statues", ["artist_id"], :name => "idx_statues_artist_id"
  add_index "statues", ["category_id"], :name => "idx_statues_category_id"
  add_index "statues", ["entity_id"], :name => "idx_statues_entity_id"

  create_table "themes", :force => true do |t|
    t.string   "name",        :limit => 64, :null => false
    t.text     "description"
    t.integer  "from"
    t.integer  "to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                               :default => "", :null => false
    t.string   "encrypted_password",   :limit => 128, :default => "", :null => false
    t.string   "password_salt",                       :default => "", :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
