class DatePastValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !value.nil? && value > DateTime.now
      record.errors.add(attribute, options[:message] || I18n.translate("date_past"))
    end
  end
end