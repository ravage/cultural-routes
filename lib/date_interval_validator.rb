class DateIntervalValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !value.nil? && record.send(options[:high]) < record.send(options[:low])
      record.errors.add(options[:high], options[:message])
    end
  end
end