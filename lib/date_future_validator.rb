class DateFutureValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if if !value.nil? && value < DateTime.now
      record.errors.add(attribute, options[:message] || I18n.translate("date_future"))
    end
  end
end