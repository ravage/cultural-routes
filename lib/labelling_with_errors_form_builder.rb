# encoding: utf-8
class LabellingWithErrorsFormBuilder < ActionView::Helpers::FormBuilder
  field_types = %w[email_field file_field number_field password_field phone_field range_field telephone_field text_area text_field url_field]
  
  field_types.each do |method|
    define_method(method) do |attribute, *args|
      options = args.extract_options!
      output = required(attribute, options)
      options[:class] = "field-with-errors" if errors?(attribute)
      options.delete(:example)
      options.delete(:with_label)
      output += super(attribute, options)
      output += display_errors(attribute)
    end
  end
  
  def collection_select(attribute, collection, value_method, text_method, options = {}, html_options = {}) 
    output = required(attribute, options)
    output += super(attribute, collection, value_method, text_method, options, html_options)
    output += display_errors(attribute)
  end
  
  private
  def required(attribute, options)
    if @object.class.validators_on(attribute).map(&:class).include?(ActiveModel::Validations::PresenceValidator)
      required = @template.content_tag("span", I18n.t(:required), :class => "required")
      output = label(attribute, "#{@object.class.human_attribute_name(attribute)} #{required} #{example(options)}".html_safe)
    else
      output = label(attribute, "#{@object.class.human_attribute_name(attribute)} #{example(options)}".html_safe, :class => "generated")
    end
  end
  
  def errors?(attribute)
    @object.errors[attribute].any?
  end
  
  def display_errors(attribute)
    @template.content_tag("span", @object.errors[attribute].first, :class => "field-error") if @object.errors[attribute].any?
  end
  
  def example(options)
    @template.content_tag("span", options[:example], :class => "example") if options.key?(:example)
  end
end