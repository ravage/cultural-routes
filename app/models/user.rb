class User < ActiveRecord::Base
  default_scope :order => 'email ASC'
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable, :lockable and :timeoutable
  devise :database_authenticatable, :recoverable, :trackable, :validatable, :timeoutable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation
  
  def to_param
    "#{id}-#{email.parameterize}"
  end
end
