class Block < ActiveRecord::Base
  default_scope order("weight ASC")
  belongs_to :page
  validates_presence_of :title, :content 
  def to_param
    "#{id}-#{title.parameterize}"
  end

end
