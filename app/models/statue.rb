# encoding: utf-8
class Statue < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  default_scope :order => 'title ASC'
  belongs_to :artist
  belongs_to :author
  belongs_to :entity
  belongs_to :category
  has_many :assets, :as => :assetable
  
  validates_presence_of :title
  validates_presence_of :description
  validates_presence_of :location
  validates_presence_of :artist_id
  validates_presence_of :entity_id, :if => Proc.new { |a| a.author_id.blank? }
  validates_presence_of :author_id, :if => Proc.new { |a| a.entity_id.blank? }
  validates_presence_of :category_id
  
  validates_numericality_of :date, :only_integer => true, :less_than_or_equal_to => Time.now.year,
     :unless => Proc.new { |a| a.date_before_type_cast.blank? }
  

  def to_api
    statues = @attributes
    statues["artist"] = artist.name
    statues["category"] = category.name
    statues["entity"] = represented.name
    statues["asset"] = assets.empty? ? "" : assets.first.asset.url(:thumb)
    statues["url"] = "/monuments/statues/#{to_param}" 
    statues["artist_url"] = "/monuments/artists/#{artist.to_param}"
    statues["entity_url"] = represented_path
    statues
  end

  def represented
    entity_id.blank? ? author : entity    
  end

  def represented_path
    path = send("entity_path", represented)
  end

  def random_asset
    assets[rand(assets.length)].asset if assets.any?
  end

  def to_param
    "#{id}-#{title.parameterize}"
  end

end
