class Book < ActiveRecord::Base
  default_scope :order => 'title ASC'
  
  has_many :citations
  has_and_belongs_to_many :authors
  
  validates_numericality_of :date, :only_integer => true, :less_than_or_equal_to => Time.now.year
    
  validates_presence_of :title, :date, :author_ids
  
  def reference
    author = authors.first
    names = author.name.split()
    last_name = names.pop
    first_name = names.join(", ")
    ref = [last_name, first_name, title, publisher, location, date]
    ref.delete_if { |element| element.blank? }.join(", ")
  end

  def to_param
    "#{id}-#{title.parameterize}"
  end
end
