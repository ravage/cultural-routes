class Location < ActiveRecord::Base
  include ApplicationHelper
  default_scope :order => 'weight ASC'
  has_many :assets, :as => :assetable
  has_many :citations
  belongs_to :literary_route
  before_validation :ensure_name
  
  validates_presence_of :name, :location, :latitude, :longitude
  
  def title
    name
  end

  def to_param
     "#{id}-#{name.parameterize}"
  end
  
  def to_api
    data = { 
      :name => name,
      :description => parse_markdown(description || ""),
      :location => location,
      :latitude => latitude,
      :longitude => longitude
    }
  end

  private
  
  def ensure_name
    write_attribute("name", location) if name.blank?
  end

  def self.routes
    connection.select_all("SELECT DISTINCT ON (literary_route_id) * FROM locations;")
  end
end
