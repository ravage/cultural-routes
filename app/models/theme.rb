class Theme < ActiveRecord::Base
  default_scope :order => 'name ASC'
  has_and_belongs_to_many :literary_routes
  
  validates_presence_of :name
  
  
  validates_numericality_of :from, :only_integer => true, :less_than_or_equal_to => Time.now.year,
    :unless => Proc.new { |a| a.from_before_type_cast.blank? }

  validates_numericality_of :to, :only_integer => true, :less_than_or_equal_to => Time.now.year,
    :unless => Proc.new { |a| a.to_before_type_cast.blank? }

  validates_numericality_of :from, :less_than_or_equal_to => Proc.new { |a| a.to }, 
    :if => Proc.new { |a| !a.to.nil? && !a.from_before_type_cast.blank?} 

  def to_param
    "#{id}-#{name.parameterize}"
  end
end
