class Category < ActiveRecord::Base
  default_scope :order => 'name ASC'
  
  has_many :statues
  validates_presence_of :name
  @@per_page = 20
  
  def self.per_page
    return @@per_page
  end
  
  def to_param
    "#{id}-#{name.parameterize}"
  end
end
