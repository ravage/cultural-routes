class Page < ActiveRecord::Base
  has_many :blocks
  
 validates_presence_of :name

  def to_param
    "#{id}-#{name.parameterize}"
  end
end
