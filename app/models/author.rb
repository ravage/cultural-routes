class Author < ActiveRecord::Base
  default_scope :order => 'name ASC'
  
  has_many :literary_routes
  has_and_belongs_to_many :books
  has_many :statues
  
  validates_presence_of :name
  
  validates_numericality_of :birthdate, :only_integer => true, :less_than_or_equal_to => Time.now.year,
    :unless => Proc.new { |a| a.birthdate_before_type_cast.blank? }

  validates_numericality_of :deathdate, :only_integer => true, :less_than_or_equal_to => Time.now.year,
    :unless => Proc.new { |a| a.deathdate_before_type_cast.blank? }

  validates_numericality_of :birthdate, :less_than_or_equal_to => Proc.new { |a| a.deathdate }, 
    :if => Proc.new { |a| !a.deathdate.nil? && !a.birthdate_before_type_cast.blank?} 
    
  
  def random_statue
    statues[rand(statues.length)]
  end

  def self.initials
    connection.select_all("SELECT * FROM initials_for('#{self.table_name}', 'name') as letter").map { |row| row["letter"] }
  end

  def to_param
    "#{id}-#{name.parameterize}"
  end
end
