class Contact 
  include ActiveModel::AttributeMethods
  include ActiveModel::Validations
  include ActiveModel::Conversion  
  extend ActiveModel::Translation
  extend ActiveModel::Naming

  attr_accessor :name, :email, :subject, :message
  #attr_reader :errors

  validates_presence_of :message
  validates :email, :email => { :message => I18n.t(:invalid_email, :scope => "errors.messages") }, :unless => lambda { |a| a.email.blank? } 

  def initialize(attributes = {})
    attributes.each do |key, value|
      self.send("#{key}=", value)
    end
    @attributes = attributes
  end

  def save(type = :contact)
    if self.valid?
      if type == :contact
        ContactMailer.contact(self).deliver
      elsif type == :support
        ContactMailer.support(self).deliver
      end
      return true
    end
    return false
  end

  def persisted?  
    false  
  end  

  def self.human_attribute_name(attr, options = {})
    I18n.translate(attr, :scope => [:activerecord, :attributes, self.model_name.downcase])
  end

  def read_attribute_for_validation(attr)
    send(attr)
  end

  def Contact.lookup_ancestors
    [self]
  end
end
