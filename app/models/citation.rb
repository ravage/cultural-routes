class Citation < ActiveRecord::Base
  include ApplicationHelper
  default_scope :order => 'citation ASC'
  belongs_to :book
  belongs_to :location
  
  validates_presence_of :citation, :book_id, :location_id
  
  def reference
    book.reference + ", p. " + pages unless pages.blank?
  end

  def to_api
    data = {
      :citation => parse_markdown(citation),
      :pages => pages,
      :reference => parse_markdown(reference)
    }
  end
end
