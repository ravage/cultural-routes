class LiteraryRoute < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  default_scope :order => 'name ASC'
  has_many :locations
  belongs_to :author
  has_and_belongs_to_many :themes
  
  validates_presence_of :name, :description
  
  accepts_nested_attributes_for :locations, :reject_if => proc { |attributes| attributes['location'].blank? }, :allow_destroy => true
  
  def to_param
    "#{id}-#{name.parameterize}"
  end

  def self.routes
    default_url_options[:host] = 'www.fragmentized.net'
    all_routes = []
    self.includes(:locations).all.each do |route|
      all_routes << {
        :name => route.name,
        :latitude => route.locations.first.latitude,
        :longitude => route.locations.first.longitude,
        :url => route.literary_route_path(route)
      } if route.locations.any?
    end
    all_routes
  end
end
