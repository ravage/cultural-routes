class Artist < ActiveRecord::Base
  default_scope :order => 'name ASC'
  
  has_many :statues
  has_many :related_works
  
  validates_presence_of :name

  validates_numericality_of :birthdate, :only_integer => true, :less_than_or_equal_to => Time.now.year,
    :unless => Proc.new { |a| a.birthdate_before_type_cast.blank? }

  validates_numericality_of :deathdate, :only_integer => true, :less_than_or_equal_to => Time.now.year,
    :unless => Proc.new { |a| a.deathdate_before_type_cast.blank? }

  validates_numericality_of :birthdate, :less_than_or_equal_to => Proc.new { |a| a.deathdate }, 
    :if => Proc.new { |a| !a.deathdate.nil? && !a.birthdate_before_type_cast.blank?}
        
  @@per_page = 20

  def self.per_page
    return @@per_page
  end

  def random_statue
    statues[rand(statues.length)]
  end
  
  def to_param
    "#{id}-#{name.parameterize}"
  end
end
