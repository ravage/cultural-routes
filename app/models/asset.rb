class Asset < ActiveRecord::Base
  include Paperclip
  
  belongs_to :assetable, :polymorphic => true
  
  attr_accessor :remote_image_url, :local_file
  
  validates_presence_of :title
  
  validates_presence_of :image_url, :unless => Proc.new { |a| a.remote_image_url.blank? }
  validates_presence_of :remote_image_url, :if => Proc.new { |a| a.local_file.blank? }
  validates_presence_of :video_url, :if => Proc.new { |a| a.media_type == "video" }, :message => :invalid
  
  has_attached_file :asset,
    :path => ":rails_root/public/system/assets/:poly/:id_partition/:style.:extension",
    :url => "/system/assets/:poly/:id_partition/:style.:extension",
    :styles => lambda { |a| self.image?(a.instance_read(:content_type)) ? { :medium => "800x600>", :thumb => "200x150#" } : {} },
    :default_url => "/system/assets/defaults/missing_:style.png"
  
  before_post_process :ensure_title_assignment, :sanitize_file_name, :change_content_type,
    :if => Proc.new { |a| a.remote_image_url.blank? && a.media_type != "video" }
  
  validates_attachment_content_type :asset, :content_type => ['image/jpeg', 'image/gif', 'image/png'], 
    :message => I18n.translate("paperclip.errors.attachment.type", :type => "gif, jpg, png")
  
  validates_attachment_size :asset, :less_than => 2.megabytes, 
    :message => I18n.translate("paperclip.errors.attachment.size", :count => 2048, :unit => "Kibibytes")
    
  before_validation :download, :unless => Proc.new { |a| a.remote_image_url.blank? }
 
  def geometry
    geometry = {}
    unless asset.to_file.nil?
      asset.styles.keys.each { |style| geometry[style] = Paperclip::Geometry.from_file(asset.to_file(style)) }
      geometry[:original] = Paperclip::Geometry.from_file(asset.to_file(:original)) 
    end
    geometry
  end
  
  def info
    errors.any? ? asset_info_error : asset_info
  end
  
  def image?
    media_type == "image"
  end
  
  def video?
    media_type == "video"
  end
  
  protected
  def asset_info
    response = {}
    response[:status] = "success"
    response[:id] = id
    response[:title] = title
    response[:type] = media_type
    response[:url] = asset.url(:medium)
    response[:thumb] = asset.url(:thumb)
    response[:width] = width
    response[:height] = height
    response[:embed] = embed_code
    response
  end
  
  def asset_info_error
    response = {}
    response[:status] = "error"
    response[:errors] = errors
    response
  end
  
  def sanitize_file_name
    extension = File.extname(asset.original_filename)
    name = title
    asset.instance_write(:file_name, "#{name.parameterize}.#{extension.parameterize}")
  end
  
  def self.image?(content_type)
    ['image/jpeg', 'image/gif', 'image/png'].include?(content_type)
  end
  
  def ensure_title_assignment
    extension = File.extname(asset.original_filename)
    self.title = File.basename(asset.original_filename, extension).titleize if title.blank?
  end
  
  def change_content_type
    mime = MIME::Types.type_for(asset.to_file.path)
    asset.instance_write(:content_type, mime.first.content_type) unless mime.nil?
  end
  
  def download
    @uri_parser ||= URI.const_defined?(:Parser) ? URI::Parser.new : URI
    if media_type == "video"
      begin
        embedly_re = Regexp.new(/http:\/\/(.*youtube\.com\/watch.*|.*\.youtube\.com\/v\/.*|youtu\.be\/.*|.*\.youtube\.com\/user\/.*#.*|.*\.youtube\.com\/.*#.*\/.*|.*justin\.tv\/.*|.*justin\.tv\/.*\/b\/.*|www\.ustream\.tv\/recorded\/.*|www\.ustream\.tv\/channel\/.*|qik\.com\/video\/.*|qik\.com\/.*|.*revision3\.com\/.*|.*\.dailymotion\.com\/video\/.*|.*\.dailymotion\.com\/.*\/video\/.*|www\.collegehumor\.com\/video:.*|.*twitvid\.com\/.*|www\.break\.com\/.*\/.*|vids\.myspace\.com\/index\.cfm\?fuseaction=vids\.individual&videoid.*|www\.myspace\.com\/index\.cfm\?fuseaction=.*&videoid.*|www\.metacafe\.com\/watch\/.*|blip\.tv\/file\/.*|.*\.blip\.tv\/file\/.*|video\.google\.com\/videoplay\?.*|.*revver\.com\/video\/.*|video\.yahoo\.com\/watch\/.*\/.*|video\.yahoo\.com\/network\/.*|.*viddler\.com\/explore\/.*\/videos\/.*|liveleak\.com\/view\?.*|www\.liveleak\.com\/view\?.*|animoto\.com\/play\/.*|dotsub\.com\/view\/.*|www\.overstream\.net\/view\.php\?oid=.*|www\.livestream\.com\/.*|www\.worldstarhiphop\.com\/videos\/video.*\.php\?v=.*|worldstarhiphop\.com\/videos\/video.*\.php\?v=.*|teachertube\.com\/viewVideo\.php.*|teachertube\.com\/viewVideo\.php.*|bambuser\.com\/v\/.*|bambuser\.com\/channel\/.*|bambuser\.com\/channel\/.*\/broadcast\/.*|www\.whitehouse\.gov\/photos-and-video\/video\/.*|www\.whitehouse\.gov\/video\/.*|wh\.gov\/photos-and-video\/video\/.*|wh\.gov\/video\/.*|www\.hulu\.com\/watch.*|www\.hulu\.com\/w\/.*|hulu\.com\/watch.*|hulu\.com\/w\/.*|movieclips\.com\/watch\/.*\/.*\/|movieclips\.com\/watch\/.*\/.*\/.*\/.*|.*crackle\.com\/c\/.*|www\.fancast\.com\/.*\/videos|www\.funnyordie\.com\/videos\/.*|www\.vimeo\.com\/groups\/.*\/videos\/.*|www\.vimeo\.com\/.*|vimeo\.com\/groups\/.*\/videos\/.*|vimeo\.com\/.*|www\.ted\.com\/talks\/.*\.html.*|www\.ted\.com\/talks\/lang\/.*\/.*\.html.*|www\.ted\.com\/index\.php\/talks\/.*\.html.*|www\.ted\.com\/index\.php\/talks\/lang\/.*\/.*\.html.*|.*omnisio\.com\/.*|.*nfb\.ca\/film\/.*|www\.thedailyshow\.com\/watch\/.*|www\.thedailyshow\.com\/full-episodes\/.*|www\.thedailyshow\.com\/collection\/.*\/.*\/.*|movies\.yahoo\.com\/movie\/.*\/video\/.*|movies\.yahoo\.com\/movie\/.*\/info|movies\.yahoo\.com\/movie\/.*\/trailer|www\.colbertnation\.com\/the-colbert-report-collections\/.*|www\.colbertnation\.com\/full-episodes\/.*|www\.colbertnation\.com\/the-colbert-report-videos\/.*|www\.comedycentral\.com\/videos\/index\.jhtml\?.*|www\.theonion\.com\/video\/.*|theonion\.com\/video\/.*|wordpress\.tv\/.*\/.*\/.*\/.*\/|www\.traileraddict\.com\/trailer\/.*|www\.traileraddict\.com\/clip\/.*|www\.traileraddict\.com\/poster\/.*|www\.escapistmagazine\.com\/videos\/.*|www\.trailerspy\.com\/trailer\/.*\/.*|www\.trailerspy\.com\/trailer\/.*|www\.trailerspy\.com\/view_video\.php.*|www\.atom\.com\/.*\/.*\/|fora\.tv\/.*\/.*\/.*\/.*|www\.spike\.com\/video\/.*|www\.gametrailers\.com\/video\/.*|gametrailers\.com\/video\/.*|www\.godtube\.com\/featured\/video\/.*|www\.tangle\.com\/view_video.*|espn\.go\.com\/video\/clip.*|espn\.go\.com\/.*\/story.*|cnbc\.com\/id\/.*|cbsnews\.com\/video\/watch\/.*|www\.cnn\.com\/video\/.*|edition\.cnn\.com\/video\/.*|money\.cnn\.com\/video\/.*|today\.msnbc\.msn\.com\/id\/.*\/vp\/.*|www\.msnbc\.msn\.com\/id\/.*\/vp\/.*|www\.msnbc\.msn\.com\/id\/.*\/ns\/.*|today\.msnbc\.msn\.com\/id\/.*\/ns\/.*|multimedia\.foxsports\.com\/m\/video\/.*\/.*|msn\.foxsports\.com\/video.*)/i)
        return if embedly_re !~ video_url
        user_agent = "Mozilla/5.0 (compatible; cultural_routes/0.1; +ravage@fragmentized.net)"
        response = open(@uri_parser.parse("http://api.embed.ly/v1/api/oembed?url=#{video_url}&maxwidth=600&format=json"),
          "User-Agent" => user_agent)
        
        data = JSON.parse(response.read)
        self.video_url = data["url"]
        self.embed_code = data["html"]
        self.width = data["width"]
        self.height = data["height"]
        self.title = data["title"] if title.blank?
        unless data.has_key?("thumbnail_url")
          self.asset = File.new("public/images/video.png")
          self.remote_image_url = nil
          self.local_file = true
          return
        end
        #data["thumbnail_url"].gsub!(/default/, "0") if data["provider_name"] == "YouTube"
        self.remote_image_url = data["thumbnail_url"]
      rescue Exception => e
        self.video_url = self.remote_image_url = nil
        return
      end
    end
    
    begin
      io = open(@uri_parser.parse(remote_image_url))
      def io.original_filename; base_uri.path.split('/').last; end
      io.original_filename.blank? ? nil : io
      self.asset = io
      self.image_url = remote_image_url
      ensure_title_assignment
      sanitize_file_name
    rescue Exception => e
      self.remote_image_url = nil
    end
  end
end
