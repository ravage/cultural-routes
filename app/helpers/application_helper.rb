# encoding: utf-8
module ApplicationHelper
  def meta_tags
    main_title(@page_title) unless @page_title.nil?
  end
  def title(page_title)
    content_for :title, page_title
  end

  def main_title(page_title)
    template = page_title
    template += " :: "
    if controller_name == "main" || controller_name == "contacts"
      template += "Geo(Referência) Cultural"
    else
      template += (request.request_uri =~ /monuments/) ? _("Estátuas") : _("Percursos Literários")
    end
    title(template)
  end
  
  def javascript(*args, &block)
    content_for :js do
      javascript_include_tag(args)
    end
    
    if block_given?
      content_for :js do
        capture(&block)
      end
    end
  end
  
  def zebra(index)
    (index % 2 == 0) ? "even" : "odd"
  end
  
  def truncate(text, *args)
    options = args.extract_options!
    options.reverse_merge!(:length => 30, :omission => "...")

    if text
      l = options[:length] - options[:omission].mb_chars.length
      chars = text.mb_chars
      (chars.length > options[:length] ? chars[0...l] + options[:omission] : text).to_s
    end
  end
  
  def localize_date(date)
    date.blank? ? '' : date.strftime(t(:default, :scope => [:date, :formats]))
  end
  
  def mark_active?(zone)
    "active" if current_page?(:controller => zone)
  end
  
  def assetable_url(*path)
    new_path = []
    path.each do |item|
      if item.is_a?(Location)
        new_path.push(item.literary_route)
      end
      new_path.push(item)
    end
    new_path
  end
  
  def assetable_url_for(*path)
    url_for(assetable_url(*path))
  end

  def parse_markdown(text)
    RDiscount.new(text, :smart, :filter_html, :safelink, :strict).to_html.html_safe unless text.nil?
  end

  def linked_abc(abc)
    ret = []
    "A".upto("Z").each do |char|
      if abc.include?(char)
        ret << link_to(char, "?search=#{char}")
      else
        ret << char 
      end
    end
    ret
  end

  def random_asset(model)
    asset = model.random_asset
    unless asset.nil?
      content_tag(:li, link_to(image_tag(asset.url(:thumb)), model))
    else
      nil
    end
  end
 end


