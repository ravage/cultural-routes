# encoding: utf-8
class ContactMailer < ActionMailer::Base
  helper :application
  
  default :to => Site.to, :from => Site.from
  
  def contact(data)
    @subject = (data.subject.blank?) ? _("Sem Assunto") : data.subject
    @name = (data.name.blank?) ? _("Anónimo") : data.name
    @message = data.message
    @email = (data.email.blank?) ? _("Desconhecido") : data.email
    mail(:reply_to => data.email, :subject => "[Contacto - #{Site.title}] #{@subject}")
  end

  def support(data)
    @data = data
    @data.subject = _("Sem Assunto") if @data.subject.blank?
    mail(:from => @data.email, :to => Site.support, :reply_to => @data.email, :subject => "[Suporte - #{Site.title}] | #{@data.subject}")
  end
end
