class AuthorsController < ApplicationController
  respond_to :html

  def show
   @author = Author.find(params[:id]) 
   @page_title = @author.name
   respond_with(@author)
  end

  def index
    if params.include?(:search)
      @authors = Author.where("name LIKE ?", "#{params[:search]}%")
    else
      @authors = Author.limit(50)
    end
    @page_title = _("Autores")
    respond_with(@authors)
  end
end
