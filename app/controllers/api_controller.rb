class ApiController < ApplicationController
  respond_to :json, :xml
  respond_to :html, :only => :assets
  layout nil

  def artist
    @artists = Artist.where("name ILIKE ?", "%#{params[:term]}%").all.map do |o|
      { :id => o.id, :value => o.name, :label => o.name }
    end
    
    respond_with(@artists)
  end

  def statues_by_artist
    @statues = Statue.where("artist_id = ?", params[:id]).includes(:assets, :category, :artist, :entity).all
    respond_with(@statues) do |format|
      format.json { render :json => @statues.map { |statue| statue.to_api } }
    end
  end

  def statues_by_category
    @statues = Statue.where("category_id = ?", params[:id]).all
    respond_with(@statues) do |format|
      format.json { render :json => @statues.map { |statue| statue.to_api } }
    end
  end

  def statues_by_entity
    @statues = Statue.where("entity_id = ?", params[:id]).all
    respond_with(@statues) do |format|
      format.json { render :json => @statues.map { |statue| statue.to_api } }
    end
  end

  def statues_by_author
    @statues = Statue.where("author_id = ?", params[:id]).all
    respond_with(@statues) do |format|
      format.json { render :json => @statues.map { |statue| statue.to_api } }
    end
  end

  def statue
    @statue = Statue.find(params[:id])
    respond_with(@statue) do |format|
      format.json { render :json => @statue.to_api }
    end
  end

  def assets
    @asset = Asset.find(params[:id])
    respond_with(@asset)
  end
  
  def routes
    @routes = LiteraryRoute.routes
    respond_with(@routes)
  end

  def route
    @locations = LiteraryRoute.find(params[:id]).locations
    respond_with(@locations)
  end

  def location_detail 
    @location = Location.find(params[:id])
    @assets = @location.assets.collect { |asset| asset.info }
    @citations = @location.citations.collect { |citation| citation.to_api }

    @response = { :location => @location.to_api, :assets => @assets, :citations => @citations }
    
    render :json => @response
    #respond_with(@response)
  end

  def location
    @location = Location.find(params[:id])
    respond_with(@location)
  end
end
