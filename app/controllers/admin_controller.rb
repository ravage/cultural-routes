class AdminController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  
  def index
    @sent = false;
    if request.post?
      @contact = Contact.new(params[:contact])
      @sent = @contact.save(:support)
    else
      @contact = Contact.new
    end
  end
  
  def preview
    if params.has_key?(:data)
      markdown = RDiscount.new(params[:data], :smart, :filter_html, :safelink, :strict)
      render :text => markdown.to_html
    else
      render :text => ""
    end
  end
end
