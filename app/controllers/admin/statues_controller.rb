# encoding: utf-8

class Admin::StatuesController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js, :json
  
  def index
    @statues = Statue.includes(:assets).paginate :page => params[:page], :per_page => 10
    respond_with(:admin, @statues)
  end
  
  def show
    @statue = Statue.find(params[:id])
    respond_with(@statue);
  end
  
  def new
    @statue = Statue.new
    @asset = @statue.assets.new
  end
  
  def create
    @statue = Statue.new(params[:statue])
    if @statue.save
      flash.now[:notice] = _("Registo criado com sucesso")
    end
    respond_with(:admin, @statue, :location => [:new, :admin, @statue, :asset])
  end
  
  def edit
    @statue = Statue.find(params[:id])
    @assets = @statue.assets.paginate :page => params[:page], :per_page => 10
    @asset = @statue.assets.new
    respond_with(@statue)
  end
  
  def update
    @statue = Statue.find(params[:id])
    if @statue.update_attributes(params[:statue])
      flash[:notice] = "Registo alterado com sucesso!"
    end
    respond_with(@statue, :location => [:admin, :statues])
  end
  
  def destroy
    affected = Statue.delete(params[:id])
    if affected > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def delete
    if Statue.delete_all(:id => params[:statue]) > 0
      redirect_to :back, :notice => "Registos removidos com sucesso"
    else
      flash[:error] = "Erro ao remover registos"
      redirect_to :back
    end
  end
end
