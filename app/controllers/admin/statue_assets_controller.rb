# encoding: utf-8
class Admin::StatueAssetsController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js, :json
  #skip_before_filter :verify_authenticity_token, :only => [:create]
  before_filter :get_statue, :except => [:destroy]
  
  def index
    @assets = @statue.statue_assets.paginate :page => params[:page]
    respond_with(:admin, @statue, @assets)
  end
  
  def new
    @asset = @statue.assets.new
    respond_with(@asset)
  end
  
  def edit
    @asset = @statue.statue_assets.find(params[:id])
    @geometry = @asset.geometry
    respond_with(@asset)
  end
  
  
  def create
    params[:statue_asset][:media_type] = params[:media_type] if params.has_key?(:media_type)
    params[:statue_asset][:local_file] = params[:local_file] if params.has_key?(:local_file)
    
    @asset = @statue.statue_assets.new(params[:statue_asset]);
    if @asset.save
     flash.now[:notice] = _("Registo criado com sucesso")
    end
    puts @asset.errors
    if params.has_key?(:data_type) && params[:data_type] == "json" || request.headers["HTTP_USER_AGENT"] =~ /Adobe Flash/
        render :text => @asset.info and return
    else
        respond_with(@asset, :location => [:admin, @statue, :statue_assets])
    end
  end
  
  def update
    @asset = @statue.statue_assets.find(params[:id])
    if @asset.update_attributes(params[:statue_asset])
      flash[:notice] = "Registo alterado com sucesso!"
    end
    respond_with(@asset, :location => [:admin, @statue, :statue_assets])
  end
  
  def destroy
    @asset = StatueAsset.find(params[:id])
    @asset.destroy
    if @asset.destroyed?
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def delete
    StatueAsset.destroy_all(:id => params[:statue_asset])
    redirect_to :back, :notice => "Registos removidos com sucesso"
  end
  
  def form
    @asset = @statue.statue_assets.new
  end
  
  private
  
  def get_statue
    @statue = Statue.find(params[:statue_id])
  end
end