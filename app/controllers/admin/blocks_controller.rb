class Admin::BlocksController < ApplicationController
  respond_to :html
  layout "admin"
  before_filter :get_page, :authenticate_user!


  def index
    @block = @page.blocks.find(params[:id])
    respond_with(@block)
  end

  def edit
    @block = @page.blocks.find(params[:id])
    respond_with(@block)
  end

  def update
    @block = @page.blocks.find(params[:id])
    if @block.update_attributes(params[:block])
      flash.now[:notice] = "Bloco Alterado Com Sucesso!" 
    end
    respond_with(@block, :location => [:admin, @page])
  end
  private

  def get_page
    @page = Page.find(params[:page_id]) 
  end
end
