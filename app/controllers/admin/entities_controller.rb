# encoding: utf-8

class Admin::EntitiesController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js

  def index
    @entities = Entity.paginate :page => params[:page]
    respond_with(:admin, @entities)
  end

  def edit
    @entity = Entity.find(params[:id])
    respond_with(@entity)
  end

  def new
    @entity = Entity.new
    respond_with(:admin, @entity) do |format|
      format.js { render :partial => "shared/entity_form" }
    end
  end

  def create
    @entity = Entity.new(params[:entity])
    if @entity.save
      flash[:notice] = "Registo criado com sucesso!"
    end
    respond_with(:admin, @entity, :location => [:admin, :entities])
  end

  def update
    @entity = Entity.find(params[:id])
    if @entity.update_attributes(params[:entity])
      flash[:notice] = "Registo alterado com sucesso!"
    end
    respond_with(:admin, @entity, :location => [:admin, :entities])
  end

  def destroy
    affected = Entity.delete(params[:id])
    if affected > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to [:admin, :entities]
  end

  # delete many
  def delete
    if Entity.delete(params[:entities]) > 0
      redirect_to :back, :notice => "Registos removidos com sucesso"
    else
      flash.now[:error] = "Erro ao remover registos"
      redirect_to :back
    end
  end

  private
end