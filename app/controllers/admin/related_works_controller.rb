# encoding: utf-8
class Admin::RelatedWorksController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js
  before_filter :get_artist, :except => [:destroy]
  
  def index
    @related_works = @artist.related_works.paginate :page => params[:page]
    respond_with(:admin, @artist, @related_works)
  end
  
  def edit
    @related_work = @artist.related_works.find(params[:id])
    respond_with(@related_work)
  end
  
  def new
    @related_work = @artist.related_works.new
    respond_with(:admin, @artist, @related_work)
  end
  
  def create
    @related_work = @artist.related_works.new(params[:related_work])
    if @related_work.save
      flash.now[:notice] = "Registo criado com sucesso!"
    end
    respond_with(:admin, @artist, @related_work, :location => [:admin, @artist, :related_works])
  end
  
  def update
    @related_work = @artist.related_works.find(params[:id])
    if @related_work.update_attributes(params[:related_work])
      flash[:notice] = "Registo alterado com sucesso!"
    end
    respond_with(:admin, @artist, @related_work, :location => [:admin, @artist, :related_works])
  end
  
  def destroy
    affected = RelatedWork.delete_all(:artist_id => params[:artist_id], :id => params[:id])
    if affected > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  # delete many
  def delete
    if RelatedWork.delete_all(:artist_id => params[:artist_id], :id => params[:related_work]) > 0
      redirect_to :back, :notice => "Registos removidos com sucesso"
    else
      flash.now[:error] = "Erro ao remover registos"
      redirect_to :back
    end
  end
  
  private
  
  def get_artist
    @artist = Artist.find(params[:artist_id])
  end
end