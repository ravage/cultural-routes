# encoding: utf-8
class Admin::AssetsController < ApplicationController
  include ApplicationHelper
  before_filter :authenticate_user!
  
  layout "admin", :except => [:asset]
  respond_to :html, :js, :json
  #skip_before_filter :verify_authenticity_token, :only => [:create]
  before_filter :get_assetable, :except => [:destroy, :form, :asset]
  
  def index
    @assets = @assetable.assets.paginate :page => params[:page]
    respond_with(:admin, @assetable, @assets)
  end
  
  def new
    @asset = @assetable.assets.new
    respond_with(@asset)
  end
  
  def edit
    @asset = @assetable.assets.find(params[:id])
    @geometry = @asset.geometry
    respond_with(@asset)
  end
  
  
  def create
    params[:asset][:media_type] = params[:media_type] if params.has_key?(:media_type)
    params[:asset][:local_file] = params[:local_file] if params.has_key?(:local_file)
    
    @asset = @assetable.assets.new(params[:asset]);
    if @asset.save
     flash.now[:notice] = _("Registo criado com sucesso")
    end
    if request.headers["HTTP_USER_AGENT"] =~ /Adobe Flash/ && @asset.errors.any?
      render :text => "error", :status => :not_acceptable and return
    elsif params.has_key?(:data_type) && params[:data_type] == "json" || request.headers["HTTP_USER_AGENT"] =~ /Adobe Flash/
      render :json => @asset.info and return
    else
      respond_with(@asset, :location => [:admin, @assetable, :assets])
    end
  end
  
  def update
    @asset = @assetable.assets.find(params[:id])
    @geometry = @asset.geometry
    if @asset.update_attributes(params[:asset])
      flash.now[:notice] = "Registo alterado com sucesso!"
    end
    respond_with(@asset, :location => assetable_url(:admin, @assetable, :assets))
  end
  
  def destroy
    @asset = Asset.find(params[:id])
    @asset.destroy
    if @asset.destroyed?
      flash.now[:notice] = _("Registo apagado com sucesso!")
    else
      flash.now[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def delete
    Asset.destroy_all(:id => params[:asset])
    redirect_to :back, :notice => "Registos removidos com sucesso"
  end
  
  def asset
    @asset = Asset.find(params[:id])
    respond_with(@asset)
  end
  
  private
  
  def get_assetable
    @assetable = find_assetable
  end
  
  def find_assetable
    params.each do |name, value|   
      if name =~ /(location|statue)_id$/
        return $1.classify.constantize.find(value)
      end
    end
    nil
  end
  
end
