# encoding: utf-8
class Admin::CategoriesController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js
  
  def index
    @categories = Category.paginate :page => params[:page]
    respond_with(@categories)
  end
  
  def new
    @category = Category.new
    respond_with(:admin, @category) do |format|
      format.js { render :partial => "shared/category_form" }
    end
  end
  
  def create
    @category = Category.new(params[:category])
    if @category.save
      flash.now[:notice] = "Categoria criada com sucesso!"
    end
    respond_with(@category, :location => [:admin, :categories])
  end
  
  def edit
    @category = Category.find(params[:id])
  end
  
  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category])
      flash.now[:notice] = "Categoria alterada com sucesso!"
    end
    respond_with(@category, :location => [:admin, :categories])
  end
  
  def destroy
    category = Category.find(params[:id])
    category.destroy
    redirect_to [:admin, :categories], :notice => "Registo removido com sucesso!"
  end
  
  # delete many
  def delete
    if Category.delete(params[:categories]) > 0
      redirect_to [:admin, :categories], :notice => "Registos removidos com sucesso"
    else
      redirect_to [:admin, :categories]
    end
  end
end
