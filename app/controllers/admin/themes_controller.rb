class Admin::ThemesController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js
  
  def index
    @themes = Theme.paginate :page => params[:page]
    respond_with(@themes)
  end
  
  def new
    @theme = Theme.new
    respond_with(@theme)
  end
  
  def create
    @theme = Theme.new(params[:theme])
    if @theme.save
      flash[:notice] = _("Registo criado com sucesso")
    end
    respond_with(:admin, @theme, :location => [:admin, :themes])
  end
  
  def edit
    @theme = Theme.find(params[:id])
    respond_with(@theme)
  end
  
  def update
    @theme = Theme.find(params[:id])
    if @theme.update_attributes(params[:theme])
      flash[:notice] = "Registo alterado com sucesso"
    end
    respond_with(:admin, @theme, :location => [:admin, :themes])
  end
  
  def destroy
    if Theme.delete(params[:id]) > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def delete
    if Theme.delete(params[:theme]) > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
end
