class Admin::UsersController < ApplicationController
  before_filter :authenticate_user!
  layout "admin"
  respond_to :html

  def new
    @user = User.new
    respond_with(@user)
  end
  
  def create
    @user = User.new(params[:user])
    
    if @user.save
      if @user.respond_to?(:confirm!)
        flash.now[:success] = t('devise.confirmations.send_instructions')
        sign_in @user if @user.class.confirm_within > 0
      else
        flash.now[:success] = t('flash.users.create.notice')
      end
    end
    respond_with(@user)
  end
  
  def edit
    @user = current_user
    respond_with(@user)
  end

  def update
    @user = current_user
    if @user.update_attributes(params[:user])
      flash.now[:success] = "Registo actualizado com sucesso"
    end
    respond_with(@user, :location => [:admin])
  end

  def destroy
    current_user.destroy
    sign_out current_user
    flash.now[:success] = t('flash.users.destroy.notice')
    respond_with(@user)
  end
end
