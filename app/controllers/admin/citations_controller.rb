class Admin::CitationsController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js
  
  def index
    @citations = Citation.paginate :page => params[:page]
    respond_with(@citations)
  end
  
  def new
    @citation = Citation.new
    respond_with(@citation)
  end
  
  def create
    @citation = Citation.new(params[:citation])
    if @citation.save
      flash[:notice] = _("Registo criado com sucesso")
    end
    respond_with(:admin, @citation, :location => [:admin, :citations])
  end
  
  def edit
    @citation = Citation.find(params[:id])
    respond_with(@citation)
  end
  
  def update
    @citation = Citation.find(params[:id])
    if @citation.update_attributes(params[:citation])
      flash[:notice] = "Registo alterado com sucesso"
    end
    respond_with(:admin, @citation, :location => [:admin, :citations])
  end
  
  def destroy
    if Citation.delete(params[:id]) > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def delete
    if Citation.delete(params[:citation]) > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
end
