class Admin::ArtistsController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :json, :js
  
  def index
    @artists = Artist.paginate :page => params[:page]
    respond_with(@artists)
  end
  
  def new
    @artist = Artist.new
    respond_with(:admin, @artist) do |format|
      format.js { render :partial => "shared/artist_form" }
    end
  end
  
  def create
    @artist = Artist.new(params[:artist])
      if @artist.save
      flash.now[:notice] = "Artista criado com sucesso!"
    end
    respond_with(@artist, :location => [:admin, :artists])
  end
  
  def edit
    @artist = Artist.find(params[:id])
    @related_work = @artist.related_works.new
    @related_works = @artist.related_works.paginate :page => params[:page]
  end
  
  def update
    @artist = Artist.find(params[:id])
    @related_work = RelatedWork.new
    @related_works = @artist.related_works
    if @artist.update_attributes(params[:artist])
      flash.now[:notice] = "Artista alterada com sucesso!"
    end
    respond_with(@artist, :location => [:admin, :artists])
  end
  
  def destroy
    category = Artist.find(params[:id])
    category.destroy
    redirect_to [:admin, :artists], :notice => "Registo removido com sucesso!"
  end
  
  # delete many
  def delete
    if Artist.delete(params[:artists]) > 0
      redirect_to [:admin, :artists], :notice => "Registos removidos com sucesso"
    else
      redirect_to [:admin, :artists]
    end
  end
end
