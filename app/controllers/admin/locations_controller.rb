class Admin::LocationsController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js, :json
  
  before_filter :get_route
  
  def index
    @locations = @route.locations.includes(:assets).paginate :page => params[:page]
    respond_with(@locations)
  end
  
  def new
    @location = @route.locations.new
    respond_with(@location)
  end
  
  def show
    @location = @route.locations.find(params[:id])
    respond_with(@location);
  end
  
  
  def create
    @location = @route.locations.new(params[:location])
    if @location.save
      flash.now[:notice] = _("Registo criado com sucesso")
    end
    respond_with(:admin, @location, :location => [:admin, @route, :locations])
  end
  
  def edit
    @location = @route.locations.find(params[:id])
    respond_with(@location)
  end
  
  def update
    @location = @route.locations.find(params[:id])
    if @location.update_attributes(params[:location])
      flash.now[:notice] = "Registo alterado com sucesso"
    end
    respond_with(:admin, @location, :location => [:admin, @route, :locations])
  end
  
  def destroy
    @location = @route.locations.find(params[:id])
    @location.destroy
    flash.now[:notice] = "Registo removido com sucesso"
    respond_with(@location, :location => [:admin, @route, :locations])
  end
  
  def delete
    if Location.delete(params[:location]) > 0
      flash.now[:notice] = _("Registo apagado com sucesso!")
    else
      flash.now[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back 
  end

  def bulk
    Location.update(params[:route].keys, params[:route].values)
    render :json => "" 
  end
  
  private
  
  def get_route
    @route = LiteraryRoute.find(params[:literary_route_id])
  end
end
