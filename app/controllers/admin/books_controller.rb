class Admin::BooksController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js, :json
  
  def index
    @books = Book.includes(:authors).paginate :page => params[:page]
    respond_with(@books)
  end
  
  def new
    @book = Book.new
    respond_with(@book) do |format|
      format.js { render :partial => "shared/book_form" }
    end
  end
  
  def create
    #TODO: 
    @book = Book.new(params[:book])
    if @book.save
      flash[:notice] = _("Registo criado com sucesso")
    end
    respond_with(:admin, @book, :location => [:admin, :books])
  end
  
  def edit
    @book = Book.find(params[:id])
    respond_with(@book)
  end
  
  def update
    #TODO: blargh
    params[:book][:author_ids] ||= []
    # params[:book].delete(:author_ids) if params[:book][:author_ids].empty?
    @book = Book.find(params[:id])
    if @book.update_attributes(params[:book])
      flash[:notice] = "Registo alterado com sucesso"
    end
    respond_with(:admin, @book, :location => [:admin, :books])
  end
  
  def destroy
    if Book.delete(params[:id]) > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def delete
    if Book.delete(params[:book]) > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
end
