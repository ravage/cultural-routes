# encoding: utf-8

class Admin::LocationAssetsController < ApplicationController
  before_filter :authenticate_user!
 
  layout "admin"
  respond_to :html, :js
  #skip_before_filter :verify_authenticity_token, :only => [:create]
  before_filter :get_location, :except => [:destroy]
  
  def index
    @assets = @location.location_assets.paginate :page => params[:page]
    respond_with(:admin, @route, @location, @assets)
  end
  
  def new
    @asset = @location.location_assets.new
    respond_with(@location)
  end
  
  def edit
    @asset = @location.location_assets.find(params[:id])
    respond_with(@asset)
  end
  
  
  def create
    @asset = @location.location_assets.new(params[:location_asset]);
    if @asset.save
     flash.now[:notice] = _("Registo criado com sucesso")
    end
    
    if request.headers["HTTP_USER_AGENT"] =~ /Adobe Flash/
      render :text => "ok", :status => :ok
    else
      respond_with(@asset, :location => [:admin, @route, @location, :location_assets])
    end
  end
  
  def update
    @asset = @location.location_assets.find(params[:id])
    if @asset.update_attributes(params[:location_asset])
      flash.now[:notice] = "Registo alterado com sucesso!"
    end
    respond_with(@asset, :location => [:admin, @route, @location, :location_assets])
  end
  
  def destroy
    @asset = LocationAsset.find(params[:id])
    @asset.destroy
    if @asset.destroyed?
      flash.now[:notice] = _("Registo apagado com sucesso!")
    else
      flash.now[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def delete
    LocationAsset.destroy_all(:id => params[:location_asset])
    redirect_to :back, :notice => "Registos removidos com sucesso"
  end
  
  private
  
  def get_location
    @route = LiteraryRoute.find(params[:literary_route_id])
    @location = @route.locations.find(params[:location_id])
  end
end