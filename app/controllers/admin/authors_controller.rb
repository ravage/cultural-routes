class Admin::AuthorsController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js
  
  def index
    @authors = Author.paginate :page => params[:page]
    respond_with(@authors)
  end
  
  def new
    @author = Author.new
    respond_with(@author) do |format|
      format.js { render :partial => "shared/author_form" }
    end
  end
  
  def create
    @author = Author.new(params[:author])
    if @author.save
      flash[:notice] = _("Registo criado com sucesso")
    end
    respond_with(:admin, @author, :location => [:admin, :authors])
  end
  
  def edit
    @author = Author.find(params[:id])
    respond_with(:admin, @author)
  end
  
  def update
    @author = Author.find(params[:id])
    if @author.update_attributes(params[:author])
      flash[:notice] = _("Registo alterado com sucesso")
    end
    respond_with(:admin, @author, :location => [:admin, :authors])
  end
  
  def destroy
    affected = Author.delete(params[:id])
    if affected > 0
      flash[:notice] = _("Registo apagado com sucesso!")
    else
      flash[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def books
    @books = Author.find(params[:id]).books.paginate :page => params[:page]
    flash[:author_id] = params[:id].to_i
  end
  
  def routes
    @literary_routes = Author.find(params[:id]).literary_routes.paginate :page => params[:page]
    flash[:author_id] = params[:id].to_i
  end
  
  def delete
    if Author.delete_all(:id => params[:author]) > 0
       redirect_to :back, :notice => "Registos removidos com sucesso"
     else
       flash[:error] = "Erro ao remover registos"
       redirect_to :back
     end
  end
end
