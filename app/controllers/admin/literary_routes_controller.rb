class Admin::LiteraryRoutesController < ApplicationController
  before_filter :authenticate_user!
  
  layout "admin"
  respond_to :html, :js
  
  def index
    @literary_routes = LiteraryRoute.includes(:author).includes(:locations).paginate :page => params[:page]
    respond_with(@literary_routes)
  end
  
  def new
    @literary_route = LiteraryRoute.includes(:themes).new
    @literary_route.locations.build
    @themes = Theme.all
    respond_with(@literary_route)
  end
  
  def create
    @literary_route = LiteraryRoute.new(params[:literary_route])
    if @literary_route.save
      flash.now[:notice] = _("Registo criado com sucesso")
      @route = @literary_route
      @locations = @literary_route.locations.paginate :page => params[:page]
    else
      @themes = Theme.all
    end
    respond_with(:admin, @literary_route)
  end
  
  def edit
    @literary_route = LiteraryRoute.find(params[:id])
    @locations = @literary_route.locations
    @themes = Theme.all
    respond_with(@literary_route)
  end
  
  def update
    params[:literary_route][:theme_ids] ||= []
    @themes = Theme.all
    @literary_route = LiteraryRoute.find(params[:id])
    if @literary_route.update_attributes(params[:literary_route])
      flash.now[:notice] = "Registo alterado com sucesso"
    end
    @literary_routes = LiteraryRoute.includes(:author).paginate :page => params[:page]
    respond_with(:admin, @literary_route, :location => [:admin, :literary_routes])
  end
  
  def destroy
    if LiteraryRoute.delete(params[:id]) > 0
      flash.now[:notice] = _("Registo apagado com sucesso!")
    else
      flash.now[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
  
  def delete
    if LiteraryRoute.delete(params[:literary_route]) > 0
      flash.now[:notice] = _("Registo apagado com sucesso!")
    else
      flash.now[:alert] = _("Problema ao remover registo")
    end
    redirect_to :back
  end
end
