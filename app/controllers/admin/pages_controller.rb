class Admin::PagesController < ApplicationController
  before_filter :authenticate_user!
  layout "admin"
  def show
   @page = Page.includes(:blocks).find(params[:id]) 
  end
end
