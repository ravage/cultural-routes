# encoding: utf-8
class LiteraryRoutesController < ApplicationController
  def index
    @routes = LiteraryRoute.all 
    @page_title = _("Percursos")
  end

  def show
    @route = LiteraryRoute.find(params[:id])
    @page_title = @route.name
  end
end
