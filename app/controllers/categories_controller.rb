class CategoriesController < ApplicationController
  respond_to :html

  def index
    @categories = Category.all  
    @page_title = _("Categorias")
    respond_with(@categories)
  end

  def show
    @category = Category.find(params[:id])
    @page_title = @category.name
    respond_with(@category)
  end
end
