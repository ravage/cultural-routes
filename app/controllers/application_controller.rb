class ApplicationController < ActionController::Base
  include FastGettext::Translation
  before_filter :set_locale#, :encode_params

  protect_from_forgery
  layout :layout_by_resource
  
  private
  def set_locale
    FastGettext.available_locales = ['pt','en']
    FastGettext.text_domain = 'cultural_routes'
    #session[:locale] = I18n.locale = FastGettext.set_locale(params[:locale] || session[:locale] || request.env['HTTP_ACCEPT_LANGUAGE'] || 'pt')
    I18n.locale = "pt"
    FastGettext.set_locale("pt")
    session[:locale] = "pt" 
  end
  
  # https://rails.lighthouseapp.com/projects/8994/tickets/4336-ruby19-submitted-string-form-parameters-with-non-ascii-characters-cause-encoding-errors
  def encode_params
    traverse = lambda do |object, block|
      if object.kind_of?(Hash)
        object.each_value { |o| traverse.call(o, block) }
      elsif object.kind_of?(Array)
        object.each { |o| traverse.call(o, block) }
      else
        block.call(object)
      end
      object
    end
    force_encoding = lambda do |o|
      o.force_encoding(Encoding::UTF_8) if o.respond_to?(:force_encoding)
    end
    traverse.call(params, force_encoding)
  end
  
  def layout_by_resource
    if devise_controller?
      nil
    else
      "main"
    end
  end
end
