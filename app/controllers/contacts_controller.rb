class ContactsController < ApplicationController
  layout "primary"
  respond_to :html
  def new
    @contact = Contact.new  
    @blocks = Block.where("page_id = ?", 3)
    @page_title = _("Contactos")
    respond_with(@contact)
  end

  def create
    @blocks = Block.where("page_id = ?", 3)
    @sent = false
    @contact = Contact.new(params[:contact])
    if @contact.save
      flash.now[:notice] = "Agradecemos o seu contacto!"
      @sent = true
    end
    @page_title = _("Contactos")
    respond_with(@contact)
  end
end
