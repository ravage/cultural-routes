class ArtistsController < ApplicationController
  def show
    @artist = Artist.find(params[:id])
    @page_title = @artist.name
  end

  def index
    @artists = Artist.includes(:statues => :assets)
    @page_title = _("Artistas")
  end
end
