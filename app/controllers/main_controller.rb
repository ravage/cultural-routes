class MainController < ApplicationController
  layout "primary"

  def link
    @page = Page.find_by_link(params[:link])
    raise ActiveRecord::RecordNotFound if @page.nil?
    @page_title = @page.name
    @blocks = @page.blocks
  end
end
