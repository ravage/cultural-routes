# encoding: utf-8
class StatuesController < ApplicationController
  respond_to :html
  def index
    @entities = Entity.all.concat(Author.all) 
    @page_title = _("Pesquisa")
    respond_with(@entities)
  end


  def show
    @statue = Statue.find(params[:id])
    @in_category = Statue.where("category_id = ?", @statue.category.id)
    @page_title = @statue.title
    respond_with(@statue)
  end
end
