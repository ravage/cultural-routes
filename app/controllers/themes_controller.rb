# encoding: utf-8
class ThemesController < ApplicationController
  def index
    @themes = Theme.includes(:literary_routes).all
    @page_title = _("Temáticas")
  end
end
