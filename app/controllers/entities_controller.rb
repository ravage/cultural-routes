class EntitiesController < ApplicationController
  respond_to :html

  def index
    @abc = Entity.initials()
    
    if params.has_key?(:search)
      @entities = Entity.where("name ILIKE ?", "#{params[:search]}%").includes(:statues => :assets)
      @authors = Author.where("name ILIKE ?", "#{params[:search]}%").includes(:statues => :assets)
    else
      @entities = Entity.limit(50).includes(:statues => :assets)
      @authors = Author.limit(50).includes(:statues => :assets)
    end
    @page_title = _("Entidades Representadas")
    respond_with(@entities.concat(@authors));
  end
  
  def show
    if params[:type] == "entity"
      @entity = Entity.find(params[:id])
    else
      @entity = Author.find(params[:id])
    end
    @page_title = @entity.name
    respond_with(@entity)
  end
end
