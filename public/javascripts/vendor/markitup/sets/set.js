// -------------------------------------------------------------------
// markItUp!
// -------------------------------------------------------------------
// Copyright (C) 2008 Jay Salvat
// http://markitup.jaysalvat.com/
// -------------------------------------------------------------------
// MarkDown tags example
// http://en.wikipedia.org/wiki/Markdown
// http://daringfireball.net/projects/markdown/
// -------------------------------------------------------------------
// Feel free to add more tags
// -------------------------------------------------------------------
markItUpSettings = {
	previewParserPath:	"/admin/preview",
	previewAutoRefresh: false,
	
	onShiftEnter: {
		keepDefault:false, 
		openWith:'\n\n'
	},
	
	markupSet: [
		{
			name: 'Título 1', 
			key: '1', 
			placeHolder: 'Coloque o título nesta área...', 
			closeWith: function(markItUp) { return miu.markdownTitle(markItUp, '='); } 
		},
		{
			name:'Título 2', 
			key:'2', 
			placeHolder:'Coloque o título nesta área...', 
			closeWith:function(markItUp) { return miu.markdownTitle(markItUp, '-'); } 
		},
		{
			name:'Título 3', 
			key:'3', 
			openWith:'### ', 
			placeHolder:'Coloque o título nesta área...' 
		},
		{
			name:'Título 4', 
			key:'4', 
			openWith:'#### ', 
			placeHolder:'Coloque o título nesta área...' 
		},
		{
			name:'Título 5', 
			key:'5', 
			openWith:'##### ', 
			placeHolder:'Coloque o título nesta área...' 
		},
		{
			name:'Título 6', 
			key:'6', 
			openWith:'###### ', 
			placeHolder:'Coloque o título nesta área...' 
		},
		{
			separator:'---------------' 
		},		
		{
			name:'Negrito', 
			key:'B', 
			openWith:'**', 
			closeWith:'**'
		},
		{
			name:'Itálico', 
			key:'I', 
			openWith:'_', 
			closeWith:'_'
		},
		{
			separator:'---------------' 
		},
		{
			name:'Lista Não Ordenada', 
			openWith:'- ' 
		},
		{
			name:'Lista Enumerada', 
			openWith:function(markItUp) { return markItUp.line + '. '; }
		},
		{
			separator:'---------------' 
		},
		{
			name:'Link', 
			key:'L', 
			openWith:'[', 
			closeWith:']([![Url:!:http://]!] "[![Title]!]")', 
			placeHolder:'Your text to link here...' 
		},
		{
			separator:'---------------'
		},	
		{
			name:'Citação', openWith:'> '
		},
		{
			separator:'---------------'
		},
		{
			name:'Pré-Visualização',
			call: "preview",
			className:"preview"
		}
	]
};

// mIu nameSpace to avoid conflict.
miu = {
	markdownTitle: function(markItUp, character) {
		heading = '';
		n = $.trim(markItUp.selection||markItUp.placeHolder).length;
		for(i = 0; i < n; i++) {
			heading += character;
		}
		return '\n'+heading;
	}
};