(function() {
  var self;

  function AddRoute(mapOptions) {
    self = this;
    self.initialize(mapOptions);
  }  

  AddRoute.prototype = {
    initialize: function(mapOptions) {
      self.mapOptions = mapOptions;
      self.mapOptions.locationElement = $(mapOptions.locationElement);
      self.mapOptions.geocodeButton = $(mapOptions.geocodeButton);
      self.mapOptions.addMarkerButton = $(mapOptions.addMarkerButton);
      self.mapOptions.sortable = mapOptions.sortable || false;
      self.mapOptions.form = $(mapOptions.form);
      
      self.mapElement = $(self.mapOptions.mapCanvas).googleMap({
        coordinatePrecision: self.mapOptions.coordinatePrecision,
        removeLast: (self.mapOptions.removeLast === undefined) ? false : true
      });

      self.map = self.mapElement.googleMap;

      if (mapOptions.mapOverlay)
        new Fragmentized.ListOverlay(self.map.getMap());
      
      google.maps.event.addListener(self.map.getMap(), "tilesloaded", function() { 
        self.mapOptions.mapOverlay = $(mapOptions.mapOverlay);

        self.bindEvents();

        if (!self.mapOptions.newRecord) {
          self.getRouteLocations(self.mapOptions.locationsGet + ".json");
        }

        google.maps.event.clearListeners(self.map.getMap(), "tilesloaded");
      });

    },

    getRouteLocations: function(url) {
      $.getJSON(url, function(data) {
        if (data.length) {
          for (var i = 0, length = data.length; i < length; i++) {
            self.handleIncomingMarker(data[i]);
          }
        }
        else {
          self.handleIncomingMarker(data);
        }
        self.map.directions();
      });
    },

    handleIncomingMarker: function(data) {
      for (var property in data) {
        var position = self.map.createPosition(data[property].latitude, data[property].longitude);
        var marker = self.map.createMarkerFrom(position, { draggable: true });
        marker.index = data[property].id;
        marker.old = true;
        marker.setTitle(data[property].location);
        self.map.addMarker(marker);
        self.addMarkerToList(marker);
        self.map.centerMap(position);
      }
    },

    bindEvents: function() {
      self.mapElement.bind("googlemap:geocoder:success", self.geocoderSuccessHandler);
      self.mapElement.bind("googlemap:reverse:success", self.reverseSuccessHandler);
      self.mapElement.bind("googlemap:geocoder:error", self.geocoderErrorHandler);
      self.mapElement.bind("googlemap:reverse:start", self.geocoderStartHandler);
      self.mapElement.bind("googlemap:drag:end", self.markerDragEndHandler);
      self.mapElement.bind("googlemap:geocoder:start", self.geocoderStartHandler);

      self.mapOptions.geocodeButton.click(function() {
        self.map.geocodeFromAddress(self.mapOptions.locationElement.val());
      });

      self.mapOptions.form.submit(self.formSubmitHandler);
      self.mapOptions.mapOverlay.delegate("li", "click", self.markerListClickHandler);
      self.mapOptions.mapOverlay.delegate("li > img", "click", self.deleteMarkerClickHandler);
      self.mapOptions.addMarkerButton.click(self.addMarkerButtonClickHandler);

      if (self.mapOptions.sortable && self.mapOptions.handleUpdate) {
        self.mapOptions.mapOverlay.sortable({
          placeholder: "ui-state-highlight",
          stop: function(event, ui) {

            if (ui.item.data("new"))
              return;

            var data= { route: {} };
            var children = ui.item.parent().children();
            for (var i = 0, length = children.length; i < length; i++) {
              if (!children.eq(i).data("new"))
                data['route'][children.eq(i).attr("data-id")] = { weight: i };
            }
            data[$('meta[name=csrf-param]').attr('content')] = $('meta[name=csrf-token]').attr('content');
            var url = [self.mapOptions.locationsUpdate, "/bulk.json"].join('');
            self.ajaxRequest(url, "PUT", data);
          }
        });
        $(self.mapOptions.mapOverlay).disableSelection();
      }
    },

    addMarkerToList: function(marker, newItem) {
      var locationHolder = $("<li />").addClass("clearfix").attr("data-id", marker.index).append($("<div />").text(marker.getTitle())).appendTo(self.mapOptions.mapOverlay);
      locationHolder.data("new", newItem);

      if (!self.mapOptions.removeLast) {
        locationHolder.append('<img src="/images/delete.png" alt="Delete" title="Delete">');
      }
    },

    markerListClickHandler: function(e) {
      var marker = self.map.getMarkerAt(($(this).attr("data-id")));

      if (marker !== null)
        self.map.getMap().panTo(marker.getPosition());
    }, 

    addMarkerButtonClickHandler: function(e) {
      $(this).attr("disabled", "disabled");
      var marker = null;
      var locationInfo = self.mapElement.data("locationInfo");

      if (locationInfo === null || locationInfo === undefined)
        return;

      if (self.mapOptions.removeLast && self.map.getMarkers().length >= 1) {
        marker = self.map.getMarkers()[0];
        marker.setPosition(locationInfo.geometry.location);
        marker.setTitle(locationInfo.formatted_address);
        self.mapOptions.mapOverlay.find("li > div:first").text(locationInfo.formatted_address);

        var data = {
          location: {
            latitude: marker.getPosition().lat().toFixed(self.mapOptions.coordinatePrecision),
            longitude: marker.getPosition().lng().toFixed(self.mapOptions.coordinatePrecision),
            location: marker.getTitle()
          }
        };

        data[$('meta[name=csrf-param]').attr('content')] = $('meta[name=csrf-token]').attr('content');
        var url = self.mapOptions.locationsUpdate + "/" + marker.index + ".json";
        if (self.mapOptions.handleUpdate) {
          self.ajaxRequest(url, "PUT", data);
        }
      }
      else if (!self.mapOptions.removeLast || self.map.getMarkers().length === 0) {
        marker = self.map.createMarkerFrom(locationInfo.geometry.location, { draggable: true });
        marker.setTitle(locationInfo.formatted_address);
        marker.index = self.map.getMarkers().length; 
        self.map.addMarker(marker);

        if(self.map.getMarkers().length > 1) {
          self.map.directions();
        }
        self.mapElement.data("locationInfo", null);
        self.addMarkerToList(marker, true);
      }
      e.preventDefault();
    },

    geocoderStartHandler: function(e) {
      //TODO: dry up the spinner
      $("#spot-location-search-button").hide().after('<span class="spinner">Geocoding</span>');
    },

    deleteMarkerClickHandler: function(e) {
      var parent = $(this).parent();
      var marker = self.map.getMarkerAt(parent.attr("data-id"));
      self.map.removeMarkerAt(marker.index);
      parent.remove();
      self.map.directions();

      if (!self.mapOptions.newRecord && marker.old !== undefined) {
        var data = {};
        data[$('meta[name=csrf-param]').attr('content')] = $('meta[name=csrf-token]').attr('content');
        var url = self.mapOptions.locationsUpdate + "/" + marker.index + ".json";
        self.ajaxRequest(url, "DELETE", data);
      }
    },

    ajaxRequest: function(url, requestType, data) {    
      $.ajax({
        type: requestType,
        url: url,
        data: data,
        scriptCharset: "utf-8",
        dataType: "json",
        success: function(data, textStatus, XMLHttpRequest) {
          //console.log("Success: " + data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrow) {
          //console.log(textStatus);
        },
        complete: function(XMLHttpRequest, textStatus) {
          //console.log("Complete: " + textStatus);
        },
        dataFilter: function(data, type) {
          if (type == "json") {
            if ($.trim(data) === "")
              return "{}";
            return data;
          }
        }
      });
    },

    reverseSuccessHandler: function(sender, locationInfo) {
      self.mapOptions.locationElement.val(locationInfo[0].formatted_address);
      var marker = self.mapElement.data("marker");
      marker.setTitle(locationInfo[0].formatted_address);
      self.mapElement.data("marker", null);

      self.mapOptions.mapOverlay.find("li[data-id=" + marker.index + "] div").text(marker.getTitle());

      if (self.mapOptions.handleUpdate && !self.mapOptions.newRecord && marker.old !== undefined) {
        var data = {};
        data[self.mapOptions.locationFor] = {
          latitude: marker.getPosition().lat().toFixed(self.mapOptions.coordinatePrecision),
          longitude: marker.getPosition().lng().toFixed(self.mapOptions.coordinatePrecision),
          location: marker.getTitle()
        };

        data[$('meta[name=csrf-param]').attr('content')] = $('meta[name=csrf-token]').attr('content');
        var url = self.mapOptions.locationsUpdate + "/" + marker.index + ".json";
        self.ajaxRequest(url, "PUT", data);
      }

      //TODO: dry up the spinner
      $(".spinner").remove();
      $("#spot-location-search-button").show();
    },

    geocoderSuccessHandler: function(sender, args) {
      self.mapElement.data("locationInfo", args[0]);
      self.mapOptions.addMarkerButton.removeAttr("disabled");
      //TODO: dry up the spinner
      $(".spinner").remove();
      $("#spot-location-search-button").show();
    },

    markerDragEndHandler: function(sender, event, marker) {
      var position = marker.getPosition();
      self.map.geocodeFromPosition(position);
      self.mapElement.data("marker", marker);
    },

    geocoderErrorHandler: function(sender, status) {
      $("body").message(["A resolução da localização falhou com o seguinte erro: ", status].join(""), "warning");
      $(".spinner").remove();
      $("#spot-location-search-button").show();
    },

    formSubmitHandler: function(e) {
      var markers = self.map.getMarkers();
      var newLocations = $(self.mapOptions.newLocations, this).empty();
      var coordinatePrecision = self.mapOptions.coordinatePrecision;
      for (var i = 0, length = markers.length; i < length; i++) {
        if (markers[i].old === undefined) {
          var position = markers[i].getPosition();

          var latitude = $(self.mapOptions.inputTemplate.latitude);
          var longitude = $(self.mapOptions.inputTemplate.longitude);
          var weight = $(self.mapOptions.inputTemplate.weight);

          if (self.mapOptions.inputTemplate.longitude !== undefined) {
            var location = $(self.mapOptions.inputTemplate.location);

            newLocations.append(location.val(markers[i].getTitle()).replaceAttr("name", "INDEX", markers[i].index));
          }

          newLocations.append(latitude.val(position.lat().toFixed(coordinatePrecision)).replaceAttr("name", "INDEX", markers[i].index));
          newLocations.append(longitude.val(position.lng().toFixed(coordinatePrecision)).replaceAttr("name", "INDEX", markers[i].index));       
          newLocations.append(weight.val(self.mapOptions.mapOverlay.children("[data-id=" + markers[i].index +"]").index()).replaceAttr("name", "INDEX", markers[i].index)); 
        }
      }
    }
  };
  Fragmentized.AddRoute = AddRoute;
})();
