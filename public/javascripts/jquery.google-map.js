(function($) {
  var EVENTS = {
    GEOCODER_START    : "googlemap:geocoder:start",
    GEOCODER_SUCCESS  : "googlemap:geocoder:success",
    GEOCODER_ERROR    : "googlemap:geocoder:error",
    REVERSE_START     : "googlemap:reverse:start",
    REVERSE_SUCCESS   : "googlemap:reverse:success",
    REVERSE_ERROR     : "googlemap:reverse:error",
    DRAG_END          : "googlemap:drag:end"
  };

  var geocoder,
    infoWindow = null,
    directionsService,
    directionDisplay,
    map, 
    self, 
    markers = [],
    options;

  $.fn.googleMap = function(settings) {
    geocoder = new google.maps.Geocoder();
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: true, 
      preserveViewport: true
    });
    infoWindow = new google.maps.InfoWindow();

    self = this;

    options = $.extend({}, $.fn.googleMap.defaults, settings);

    if(options.autoGeolocation)
      this.getUserLocation();

    return this.each(function() {
      var $this = $(this);
      // build element specific options
      var opts = $.meta ? $.extend({}, options, $this.data()) : options;

      var mapOptions = {
        zoom: opts.mapZoom,
        center: new google.maps.LatLng(opts.initialPosition.latitude, opts.initialPosition.longitude),
        mapTypeId: google.maps.MapTypeId[opts.mapType],
        streetViewControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        }
      };
      map = new google.maps.Map($this[0], mapOptions);
    });
  };

  $.fn.googleMap.directions = function() {
    if(markers.length < 2) {
      directionsDisplay.setMap(null);
      return;
    }

    if (directionsDisplay.getMap() === undefined || directionsDisplay.getMap() === null)
      directionsDisplay.setMap(map);

    var waypoints = [];
    for (var i = 1, length = markers.length - 1; i < length; i++) {
      waypoints.push({ location: markers[i].getPosition() });
    }

    var directionsRequest = {
      origin: markers[0].getPosition(), 
      destination: markers[markers.length - 1].getPosition(),
      travelMode: google.maps.DirectionsTravelMode.WALKING
    };

    if (waypoints.length > 0) {
      directionsRequest.waypoints = waypoints;
      directionsRequest.optimizeWaypoints = true;
    }

    directionsService.route(directionsRequest, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      }
    });
  };

  $.fn.googleMap.getUserLocation = function() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(Y.bind(this.userLocationFoundHandler, this), 
        Y.bind(this.userLocationNotFoundHandler, this));
    }
  };

  $.fn.googleMap.getMap = function() {
    return map;
  };

  /**
   * Geocodes coordinates (latitude/longitude) from a literal address
   * 
   * @param address Element that triggered the geocoding event
*/
  $.fn.googleMap.geocodeFromAddress = function(address) {
    var geocoderOptions = {
      address: address
    };
    geocoder.geocode(geocoderOptions, geocodeFromAddressCallback);
    self.trigger(EVENTS.GEOCODER_START);
  };

  function geocodeFromAddressCallback(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results.length >= 0) {
        $.fn.googleMap.centerMap(results[0].geometry.location);
        self.trigger(EVENTS.GEOCODER_SUCCESS, [results]);
      }
    } else {
      self.trigger(EVENTS.GEOCODER_ERROR, [status]);
    }
  }

  $.fn.googleMap.geocodeFromPosition = function(position) {
    var geocoderOptions = {
      location: position
    };
    geocoder.geocode(geocoderOptions, geocodeFromPositionCallback);
    self.trigger(EVENTS.REVERSE_START);
  };

  function geocodeFromPositionCallback(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[0]) {
        self.trigger(EVENTS.REVERSE_SUCCESS, [results]);
        $.fn.googleMap.directions();
      }
    } else {
      self.trigger(EVENTS.REVERSE_ERROR, [status]);
    }
  }

  $.fn.googleMap.addMarker = function(marker) {
    if (markers.length > 0 && options.removeLast) {
      var oldMarker = markers.pop();
      oldMarker.setMap(null);
    }

    markers.push(marker);
    
    if(marker.getDraggable && marker.getDraggable()) {
      google.maps.event.addListener(marker, "dragstart", function(event) {
        onMarkerDragStart(event, marker);
      });

      google.maps.event.addListener(marker, "dragend", function(event) {
        onMarkerDragEnd(event, marker);
      });
    }
    
  };

  $.fn.googleMap.setInfoWindow = function(marker, content) {
    google.maps.event.addListener(marker, 'click', function() {
      infoWindow.setContent(content);
      infoWindow.open(map, marker);
    });
  };

  $.fn.googleMap.getMarkerAt = function(index) {
    var realIndex = getRealMarkerIndex(index);
    if ( realIndex != -1 )
      return markers[realIndex];

    return null;
  };

  $.fn.googleMap.infoWindowFor = function(marker) {
    google.maps.event.trigger(marker, "click");
  };

  function onMarkerDragEnd(event, marker) {
    self.trigger(EVENTS.DRAG_END, [event, marker]);
  }

  function onMarkerDragStart(event, marker) {
    if ($.fn.googleMap.defaults.liveCoords)
      google.maps.event.addListener(marker, "drag", Y.bind(onMarkerDrag, this));
  }

  function onMarkerDrag(event) {
    if ($.fn.googleMap.defaults.liveCoords) {
      this.get("liveLatitude").setContent(event.latLng.lat().toFixed(this.get("coordinatePrecision")));
      this.get("liveLongitude").setContent(event.latLng.lng().toFixed(this.get("coordinatePrecision")));
    }
  }

  $.fn.googleMap.userLocationNotFoundHandler = function(event) {
    //handle location not found
  };

  $.fn.googleMap.userLocationFoundHandler = function(event) {
    var position = new google.maps.LatLng(event.coords.latitude, event.coords.longitude);
    this.centerMap(position);
  };

  $.fn.googleMap.centerMap = function(location) {
    map.setCenter(location);
  };

  $.fn.googleMap.popMarker = function() {
    if(markers.length > 0)
      markers.pop().setMap(null); 
  };

  $.fn.googleMap.createMarkerFrom = function(position, args) {
    var options = {
      map: map,
      position: position,
      draggable: false
    };

    var markerOptions = $.extend({}, options, args);
    return new google.maps.Marker(markerOptions);
  };

  $.fn.googleMap.createPosition = function(latitude, longitude) {
    return new google.maps.LatLng(latitude, longitude);
  };

  $.fn.googleMap.getMarkers = function() {
    return markers;
  };

  $.fn.googleMap.removeMarkerAt = function(index) {
    var realIndex = getRealMarkerIndex(index);

    if (realIndex != -1) {
      var marker = markers[realIndex];
      marker.setMap(null);
      markers.splice(realIndex, 1);
    }
  };

  $.fn.googleMap.clearOverlays = function() {
    var length = markers.length;
    for (var i = 0; i < length; i++) {
      markers[i].setMap(null);
    }
  };

  function getRealMarkerIndex(index) {
    for (var i = 0, length = markers.length; i < length; i++) {
      if (markers[i].index == index) {
        return i;
      } 
    }
    return -1;
  }

  $.fn.googleMap.defaults = {
    coordinatePrecision: 6,
    mapType: "HYBRID",
    mapZoom: 17,
    initialPosition: { latitude: 41.268357, longitude: -8.617211 },
    autoGeolocation: false,
    liveCoords: false,
    removeLast: false
  };

})(jQuery);
