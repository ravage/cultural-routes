(function() {
  var Route = {
    zIndex: 0,

    mapElement: null,

    map: null,

    mapOverlay: '',

    directions: false,

    initialize: function(options) {
      this.mapElement = $(options.mapCanvas).googleMap({mapZoom: options.mapZoom});
      this.map = this.mapElement.googleMap;
      
      Route.directions = options.hasOwnProperty("directions") ? options.directions : false;

      if (options.mapOverlay)
        new Fragmentized.ListOverlay(this.map.getMap());
      
      google.maps.event.addListener(Route.map.getMap(), "tilesloaded", function() { 
        if (options.mapOverlay) {
          Route.mapOverlay = $(options.mapOverlay);
          Route.mapOverlay.delegate("li", "click", Route.mapOverlayClickHandler);
        }

        if (options.url)
          Route.renderRoutes(options.url);

        Route.bindEvents();

        google.maps.event.clearListeners(Route.map.getMap(), "tilesloaded");
      });

    },

    bindEvents: function() {

    },

    renderRoutes: function(url) {
      $.getJSON(url, this.incomingDataHandler); 
    },

    incomingDataHandler: function(data) {
      Route.map.clearOverlays();
      if (Route.mapOverlay)
        Route.mapOverlay.empty();

      if ($.isEmptyObject(data))
        return null;
      
      var route = ($.isArray(data)) ? data[0] : data;

      if ($.isPlainObject(route) && route.hasOwnProperty("location"))
        route = route["location"];

      var position = Route.map.createPosition(route.latitude, route.longitude);
      Route.map.centerMap(position);

      if ($.isArray(data)) {
        for (var i = 0, length = data.length; i < length; i++) {
          Route.parse(data[i]);
        }
      }
      else {
        Route.parse(data);
      }
      return null;
    },

    parse: function(route) {
      if ($.isPlainObject(route) && route.hasOwnProperty("location"))
        route = route["location"];

      var position = this.map.createPosition(route.latitude, route.longitude);
      var marker = this.map.createMarkerFrom(position, {icon: "/images/marker.png"});

      marker.setTitle(route.name);
      this.map.addMarker(marker);

      if (Route.directions)
        this.map.directions();
      
      marker.index = this.map.getMarkers().length;

      google.maps.event.addListener(marker, "click", function(event) {
        Route.routeNavigation(marker.index - 1, true);
        $("#map-navigation ul").css("margin-left", -(600 * (marker.index - 1)));
      });

      if (this.mapOverlay)
        this.addMarkerToList(marker, route);

      if (marker.index == 1) {
        marker.setIcon("/images/markerh.png");
      }
      //this.addMarkerToList(marker, statue);

    },

    addMarkerToList: function(marker, route) {
      var locationHolder = $("<li />").addClass("clearfix").attr("data-marker", marker.index).attr("data-current", marker.index == 1); //.attr("data-id", marker.index);
      locationHolder.append($("<div />").text(marker.getTitle())).appendTo(this.mapOverlay);
      locationHolder.data("info", route);
      
      google.maps.event.addListener(marker, "click", function(event) {
        //StatueMap.displayStatueInfo(statue);      
        window.location = route.url;
      });
    },

    mapOverlayClickHandler: function(event) {
      //Route.displayStatueInfo($(this).data("info"));
      var item = $(this);
      var route = item.data("info");
      if (route !== null) {
        Route.map.getMap().panTo(Route.map.createPosition(route.latitude, route.longitude));
        var previous = item.parent().children("[data-current=true]");
        
        if (item.attr("data-marker") == previous.attr("data-marker"))
            return false;

        var frontMarker = Route.map.getMarkerAt(item.attr("data-marker"));
        var backMarker = Route.map.getMarkerAt(previous.attr("data-current", false).attr("data-marker"));
        
        frontMarker.setIcon("/images/markerh.png");
        frontMarker.zIndex = google.maps.Marker.MAX_ZINDEX;
        backMarker.setIcon("/images/marker.png");
        backMarker.zIndex = 200;
        item.attr("data-current", true);
      }
      return true;
    },

    routeNavigation: function(index, forceSelf) {
      index = index || 0;
      var element = $("#map-navigation").find("li").eq(index + 1);
      if (element.data("content")) {
        Route.displayRoute(element.data("content"), forceSelf);
      }
      else if (element.attr("data-id")) {
        $.getJSON(["/api/location/", element.attr("data-id"), "/detail"].join(''), function(data, status, xhr) {         
          data.index = index;
          if (!element.data("content"))
            element.data("content", data);
          
          element.text(data.location.name);
          Route.displayRoute(data, forceSelf);
        });
      }
    },

    displayRoute: function(data, forceSelf) {
      var marker = Route.map.getMarkers()[data.index];
      var previous = 0;
      
      for (var markerIndex = 0, markerLength = Route.map.getMarkers().length; markerIndex < markerLength; markerIndex++) {
        Route.map.getMarkers()[markerIndex].setIcon("/images/marker.png");
      }

      if (data.hasOwnProperty("index") && marker) {
        marker.setIcon("/images/markerh.png");

        if (!forceSelf) {
          previous = (data.index - 1) % Route.map.getMarkers().length;
          
          if (previous == -1)
            previous = Route.map.getMarkers().length - 1;
          
          Route.map.getMarkers()[previous].setIcon("/images/marker.png");
        } 
        Route.map.getMap().panTo(Route.map.createPosition(data.location.latitude, data.location.longitude));
      }
      var relatedInfo = $("#related-info");
      var fiction = relatedInfo.find("#fiction");
      var reality = relatedInfo.find("#reality");
      var gallery = relatedInfo.find("#gallery");
      
      fiction.empty();
      reality.empty();
      gallery.empty();
      
      if (data.citations) {
        for (var i = 0, length = data.citations.length; i < length; i++) {
          fiction.append(data.citations[i].citation).append(data.citations[i].reference);
        }
      }
      
      if (data.assets) {
        gallery.append(Fragmentized.Formatter.getGallery(data.assets));
        gallery.find("a").colorbox({
          opacity: 0.80
        });
      }
      reality.append(data.location.description);
    }
  };
  Fragmentized.Route = Route;
})();
