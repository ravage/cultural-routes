(function() {
  var self;

  function SwfHelper(uploadOptions) {
    self = this;
    self.uploadOptions = uploadOptions;
    self.errors = {};
    self.params = {};
    self.swfu = {};
    this.initialize();
  }

  SwfHelper.prototype = {
    initialize: function(event) {
      self.errors[SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED] = "Número máximo de ficheiros em espera atingido.";
      self.errors[SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT] = "O tamanho do ficheiro é superior ao permitido.";
      self.errors[SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE] = "Não é permitido upload de ficheiros com tamanho zero.";
      self.errors[SWFUpload.QUEUE_ERROR.INVALID_FILETYPE] = "Apenas permitido upload de imagens (.gif, .png, .jpg).";

      self.params[$("meta[name=csrf-param]").attr("content")] = $("meta[name=csrf-token]").attr("content");
      self.params[self.uploadOptions.sessionKey] = self.uploadOptions.sessionData;
      self.params["media_type]"] = "image";
      self.params["local_file"] = 1;

      var settings = {
        file_post_name: self.uploadOptions.fieldName,
        flash_url: "/flash/swfupload.swf",
        flash9_url: "/flash/swfupload_fp9.swf",
        upload_url: self.uploadOptions.uploadUrl,
        post_params: self.params,
        use_query_string: true,
        file_size_limit : "2MB",
        file_types : "*.gif;*.png;*.jpg;*.jpeg;",
        file_types_description : "All Files",
        file_upload_limit : 100,
        file_queue_limit : 0,

        debug: false,

        button_image_url: "/images/swfupload_button.png",
        button_width: "132",
        button_height: "26",
        button_placeholder_id: self.uploadOptions.buttonHolder,
        button_cursor: SWFUpload.CURSOR.HAND,

        // The event handler functions are defined in handlers.js
        swfupload_preload_handler : self.preloadHandler,
        swfupload_load_failed_handler : self.loadFailedHandler,
        swfupload_loaded_handler : self.loadedHandler,

        file_queued_handler : self.fileQueued,
        file_queue_error_handler : self.fileQueueError,
        file_dialog_complete_handler : self.fileDialogComplete,
        upload_start_handler : self.uploadStart,
        upload_progress_handler : self.uploadProgress,
        upload_error_handler : self.uploadError,
        upload_success_handler : self.uploadSuccess,
        upload_complete_handler : self.uploadComplete,
        queue_complete_handler : self.queueComplete	// Queue plugin event
      };

      self.swfu = new SWFUpload(settings);
      $(self.uploadOptions.queueHolder).delegate("img.delete", "click", self.mediaItemDeleteClickHandler);
      $("#flash-buttons button").click(self.cancelUploadsClickHandler);
    },

    mediaItemDeleteClickHandler: function(event) {
      var element = $(this);
      var fileId = element.attr("data-id");
      var file = self.swfu.getFile(fileId);
      if (file !== null && (file.filestatus == SWFUpload.FILE_STATUS.QUEUED || file.filestatus == SWFUpload.FILE_STATUS.IN_PROGRESS)) {
        self.swfu.cancelUpload(fileId);
        element.closest("div").remove();
      }
      else {
        element.closest("div").remove();
      }
    },

    preloadHandler: function() {
      $(".flash-uploader").toggleClass("hidden");
    },

    loadedHandler: function() {
    },

    loadFailedHandler: function() {
      $(".flash-uploader").toggleClass("hidden");
      $(".html-uploader .content").removeClass("hidden");
    }, 

    fileQueued: function(file) {
      self.queueFile(file);
    },


    queueFile: function(file, message) {
      var error = (message === null || message === undefined) ? false : true;
      var fileSize = file.size / 1024;
      var fileInfo = "";
      if (fileSize > 1024) {
        fileSize = fileSize / 1024;
        fileInfo = fileSize.toFixed(2) + " MiB";
      }
      else {
        fileInfo = fileSize.toFixed(2) + " KiB";
      }
      var mediaItem = $('<div class="media-item clearfix"><span class="filename left"></span></div>');
      if (error) {
        mediaItem.find(".filename").append(['<span class="field-error inline">', message, " [",file.name, " (", fileInfo, ")]", '</span>'].join(""));
      }
      else {
        mediaItem.addClass("queued");
        mediaItem.find(".filename").append(file.name + " (" + fileInfo + ")");
      }
      var mediaActions = $('<span class="media-item-actions right"></span>');
      var deleteNode = $('<img src="/images/delete.png" alt="Delete" title="Delete" class="delete">');
      deleteNode.attr("data-id", file.id);
      mediaActions.append(deleteNode);
      mediaItem.append(mediaActions);

      if (!error)
        mediaItem.append('<div class="progress clear"></div>');
      $(self.uploadOptions.queueHolder).append(mediaItem);
    },

    fileQueueError: function(file, errorCode, message) {
      self.queueFile(file, self.errors[errorCode]);
    },

    fileDialogComplete: function(numFilesSelected, numFilesQueued) {
      if (numFilesSelected > 0) {
        self.swfu.startUpload();
        $("#flash-buttons button").removeAttr("disabled");
      }
    },

    uploadStart: function(file) {
      var current = $(self.uploadOptions.queueHolder + " img[data-id=" + file.id + "]");
      current.closest("div").addClass("uploading");
    },

    uploadProgress: function(file, bytesLoaded, bytesTotal) {
      var node = $("div.progress:first");
      var containerSize = node.parent().width();
      var progress = Math.ceil((bytesLoaded / bytesTotal) * (containerSize - 10));
      node.width(progress);
    },

    uploadError: function(file, errorCode, message) {
      if (errorCode == SWFUpload.UPLOAD_ERROR.FILE_CANCELLED) {
        self.removeFileById(file.id);
      }
    },

    removeFileById: function(fileId) {
      var current = $(self.uploadOptions.queueHolder + " img[data-id=" + fileId + "]");
      if(current !== null)
        current.closest("div").remove();
    },

    uploadSuccess: function(file, serverData, response) {
      var data = $.parseJSON(serverData);
      if (data.status == "success") {
        $("#media-preview").preview(data);
      }
      else {
        $("#html-upload").formErrors(data.errors, "asset");
      }
    },

    uploadComplete: function(file) {
      self.removeFileById(file.id);
    },

    queueComplete: function(numFilesUploaded) {
      $("#flash-buttons button").attr("disabled", "disabled");
    },

    cancelUploadsClickHandler: function(event) {
      self.swfu.cancelQueue();
      $("#flash-buttons button").attr("disabled", "disabled");
    }
  };
  Fragmentized.SwfHelper = SwfHelper;
})();
