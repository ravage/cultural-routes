(function() {
  var self;
  function AssetHelper() {
    self = this;
    $("#tabs").tabs();

    var htmlUpload = $("#tabs #html-upload");

    htmlUpload.append('<input type="hidden" name="data_type" value="json">');

    htmlUpload.ajaxForm({
      dataType: "json",
      resetForm: true,
      success: this.formSuccessHandler,
      beforeSend: function() { htmlUpload.clearFormErrors(); }
    });

    $("form#external-video").delegate("a.button", "click", function() {
      $.embedly({
        url: $("form#external-video").find(".external-video-url").val(),
        successCallback: self.embedlySuccessHandler,
        invalidUrlCallback: self.embedlyInvalidHandler,
        errorCallback: self.embedlyErrorHandler
      });
    });
  }

  AssetHelper.prototype = {
    formSuccessHandler: function(data) {
      console.log("success");
      if (data.status == "success") {
        $("#media-preview").preview(data);
      }
      else {
        htmlUpload.formErrors(data.errors, "asset");
      }		
    },

    embedlySuccessHandler: function(data) {
      var form = $("form#external-video");
      form.formErrors();
      form.find(".asset-title").val(data.title);
      form.find(".asset-description").val(data.description);

      if (data.hasOwnProperty("thumbnail_url")) {
        form.find("#video-thumbnail").val(data.thumbnail_url);
      }
      else {
        form.find("#video-thumbnail").val("missing");
      }

      $("body").message("Pesquisa de dados relacionados terminada com sucesso!", "success");
    },

    embedlyInvalidHandler: function() {
      $("#external-video-tab form").formErrors({
        video_url: ["URL de integração deve ser válido. Pode obtê-lo a partir da barra de navegação do seu browser."]
      }, "asset");
    },

    embedlyErrorHandler: function() {
    }
  };
  Fragmentized.AssetHelper = AssetHelper;
})();
