var Fragmentized = {};

jQuery(function() {
  var textArea = $("textarea");
  if (textArea.length) {
    textArea.markItUp(markItUpSettings);
  }
  
	// remove error message after 5 seconds if it happens to show up
  var closeButton = $(".close-button");
  if (closeButton.length) {
    closeButton.parent().delay(5000).fadeOut(function() {
      $(this).remove();
    });
  }	

  // listen for error message close button. 
  $('body').delegate(".close-button", "click", function() {
    $(this).parent().clearQueue().fadeOut(function() {
      $(this).remove();
    });
  });

  // Handle checkbox in table head
  var tableCheckbox =  $("thead :checkbox");
  if (tableCheckbox.length) {
    tableCheckbox.click(function(e) {
      var elements = $("table tbody :checkbox");
      for (var i = 0, length = elements.length; i < length; i++) {
        elements[i].checked = this.checked;
      }
      toggleSelected(elements.closest("tr"), this.checked);
    });
  }

  // Handle checkboxes in table body
  var tableBody = $("table tbody");
  if (tableBody.length) {
    tableBody.delegate(":checkbox", "click", function() {
      var element = $(this);
      toggleSelected(element.closest("tr"), this.checked);
    });
  }
  
  // Handle dialog so we can add dependent records to a combobox
  var formAddNew = $("form a.add-new");
  if (formAddNew.length) {
    formAddNew.live("click", function(e) {
      $("#dialog").dialog("destroy");
      var url = this.href;
      var title = $(this).attr("data-title");
      var sourceId = $(this).attr("data-source");

      $.get(url, function(data) {
        $("#dialog").html(data).dialog({
          dialogClass: "custom-dialog",
          modal: true,
          resizable: false,
          draggable: false,
          width: 500,
          title: title,
          overlay: {
            opacity: 0.8, 
            background: "black"
          },
          open: function(openEvent) {
            dialogOpenHandler(openEvent, this, sourceId);
          }
        });
      }, "script");

      e.preventDefault();
    });
  }
	
  function dialogOpenHandler(e, ui, sourceId) {
		var element = $(ui);
		var form = element.find("form");
		
		form.attr("data-remote", true).live("save:success", function(e, item) {
      if ($("#" + sourceId).size() > 0)
        addItemToCombobox(sourceId, item);
      $("#dialog").dialog("close");
    });

    form.find(".cancel-button").click(function(e) {
      $("#dialog").dialog("close");
      e.preventDefault();
    });
  }

  function addItemToCombobox(combo, item) {
    var element = $("#" + combo);
    element.append(["<option value =", item.valueMember, ">", item.displayMember, "</option>"].join(""));
    element.get(0).selectedIndex = element.get(0).length - 1;
    element.change();
  }

  $("select.multi-select").live("change", function(e) {
    var element = $(this);
    var value = element.val();
    
    if (value === "" || $([".combo-item input[value=", value, "]"].join("")).size() > 0)
      return;

    var input = $('<input type="hidden">').attr("name", element.attr("data-name")).attr("value", element.attr("value"));

    var display = $("<div />").addClass("combo-item").append(element.children("option:selected").text()).append('<img class="delete" src="/images/delete.png" alt="Delete">').append(input);

    var item = $(".combo-item:last");

    if (item.size() > 0) {
      item.after(display.fadeIn());
    }
    else 
      {
        element.nextAll("a:first").after(display.fadeIn());
      }
  });

  $("form").find(".delete").live("click", function() {
    $(this).parent().fadeOut(function() {
      $(this).remove();
    });
  });

  function toggleSelected(elements, selected) {
    if (selected)
      elements.addClass("selected");
    else
      elements.removeClass("selected");
  }

  $("form").delegate("button[type=submit]", "click", function(e) {
    var listener = $(e.liveFired);
    if (listener.is("form"))
      listener.clearFormErrors();
  });

  $("body").ajaxStart(function() {
    $("button[type=submit]").spinnerStart();
  });

  $("body").ajaxComplete(function() {
    $("button[type=submit]").spinnerStop();
  });

  $("a.lightbox").live("click", function() {
    url = this.href; // this is the url of the element event is triggered from
    $.fn.colorbox({
      href: url,
      title: $(this).attr("title") || $(this).attr("original-title"),
      rel: "gallery",
      opacity: 0.80
    });
    return false;
  });

  $("a[rel=media]").colorbox({
    opacity: 0.80
  });

  $("img.tooltip").tipsy({
    live: true,
    gravity: "s",
    opacity: 0.9
  });

  $("a#gallery-select-all").click(function() {
    $("form").find("input:checkbox").attr("checked", true);
  });

  $("a#gallery-select-none").click(function() {
    $("form").find("input:checkbox").attr("checked", false);
  });
});
