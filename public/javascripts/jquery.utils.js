(function($) {
	$.fn.replaceAttr = function(attribute, oldString, newString) {
		return this.each(function() {
			var $this = $(this);
			$this.attr(attribute, function() {
				return $this.attr(attribute).replace(oldString, newString);
			});
		});
	};
})(jQuery);