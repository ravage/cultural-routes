(function($) {
	$.fn.message = function(msg, type) {
		if ($("." + type).size() != 0) 
			return this;
			
		return this.each(function() {
			$("<div />")
				.addClass(type)
				.attr("id", "message-bar")
				.append(msg)
				.append('<img src="/images/cross.png" class="close-button" alt="Close">')
				.appendTo(this)
				.delay(5000)
				.fadeOut(function() {
					$(this).remove();
				});
	    });
	};
})(jQuery);

(function($) {
	$.fn.formErrors = function(errors, prefix) {
		return this.each(function() {
			var objectName = prefix;
			$(".field-error", this).remove();	
			$(".field-with-errors", this).removeClass("field-with-errors");
			for (var property in errors) {
				var id;
				if (objectName != "") {
					id = ["#", objectName, "_", property].join("");
				} 
				else {
					id = ["#", property].join("");
				}

				var current = $(this).find(id);

				if (!current.is("select") && !current.is("div"))
					current.addClass("field-with-errors");

				current.parent().append('<span class="field-error">' + errors[property][0] + '</span>');
				$("body").message("Oops! Foram detectados alguns erros no formulário. Por favor verifique os campos assinalados.", "validation");
			}
		});
	};
	
	$.fn.clearFormErrors = function() {
		$(".field-error", this).remove();	
		$(".field-with-errors", this).removeClass("field-with-errors");
	};
})(jQuery);

(function($) {
	// $.fn.spinner = function(msg) {
	// 	var message = msg || $("meta[name=loading]").attr("content");
	// 	return this.each(function() {
	// 		var $this = $(this);
	// 		var spinning = $this.data("spinning");
	// 		if (spinning == null || spinning == false) {
	// 			$this.data("spinning", true);
	// 			$this.hide().after($("<span />").addClass("spinner").text(message));
	// 		}
	// 		else
	// 		{
	// 			$this.data("spinning", false);
	// 			$this.next("span.spinner").remove();
	// 			$this.show();
	// 		}
	// 	});
	// };
	$.fn.spinnerStart = function(msg) {
		var message = msg || $("meta[name=loading]").attr("content");
		return this.each(function() {
			var $this = $(this);
			var spinning = $this.data("spinning");
			if (spinning == null || spinning === false) {
				$this.data("spinning", true);
				$this.hide().after($("<span />").addClass("spinner").text(message));
			}
		});
	};
	
	$.fn.spinnerStop = function() {
		return this.each(function() {
			var $this = $(this);
			var spinning = $this.data("spinning");
			if (spinning === true) {
				$this.data("spinning", false);
				$this.next("span.spinner").remove();
				$this.show();
			}
		});
	};
})(jQuery);

(function($) {
	$.fn.preview = function(data) {
		return this.each(function() {
			var $this = $(this);
			if (data.type == "image") {
				var link = $("<a>")
					.attr("href", data.url)
					.attr("rel", "gallery")
					.attr("title", data.title)
					.addClass("lightbox");
				
				var img = $("<img>")
					.attr("src", data.thumb)
					.attr("alt", data.title)
					.attr("title", data.title);
				
				link.append(img).fadeIn().appendTo(this);
			}
			else if (data.type == "video") 
			{
				var link = $("<a>")
					.attr("href", ["/admin/assets/", data.id].join(""))
					.attr("title", data.title)
					.addClass("lightbox");
				
				var img = $("<img>")
					.attr("src", data.thumb)
					.attr("alt", data.title)
					.attr("title", data.title);
				
				link.append(img).fadeIn().appendTo(this);
			}
			// $("a.lightbox").fancybox({
			// 	overlayOpacity: 0.5
			// });
		});
	};
})(jQuery);