(function() {
  var StatueMap = {
    mapElement: null,

    map: null,

    mapOverlay: '',

    infoWindow: new google.maps.InfoWindow({ minWidth: 300 }),

    initialize: function(options) {
      StatueMap.mapElement = $(options.mapCanvas).googleMap();
      StatueMap.map = StatueMap.mapElement.googleMap;
      
      if (options.mapOverlay)
        new Fragmentized.ListOverlay(this.map.getMap());

      google.maps.event.addListener(StatueMap.map.getMap(), "tilesloaded", function() { 
        if (options.mapOverlay) {
          StatueMap.mapOverlay = $(options.mapOverlay);
          StatueMap.mapOverlay.delegate("li", "click", StatueMap.mapOverlayClickHandler);
        }

        if (options.url)
          StatueMap.renderStatues(options.url);

        $("#statue-preview").hide();

        StatueMap.bindEvents();

        google.maps.event.clearListeners(StatueMap.map.getMap(), "tilesloaded");
      });
    },

    bindEvents: function(attribute) {
      $("#artist-search").autocomplete({
        source: "/search/artists",
        minLength: 2,
        select: function(event, ui) {
          //console.log(ui.item)
        }    
      });

      $("dd.combo").find("select").combobox({
        selected: autocompleteSelectHandler
      });

      $("dd.combo").find("input[role=textbox]").val("");
      function autocompleteSelectHandler(event, ui) {
        $("dd.combo").find("input[role=textbox]").val("");
        var element = $(event.target);

        //if (element.data("current") == element.val())
          //return false;

        //element.data("current", element.val());
        var statueMap = Fragmentized.StatueMap;
        
        switch(element.attr("id")) {
          case 'artist_id':
            statueMap.renderStatues(["/api/statues/artist/", $(ui.item).val()].join(""));
          break;
          case 'category_id':
            statueMap.renderStatues(["/api/statues/category/", $(ui.item).val()].join(""));
          break;
          case 'entity_id':
            var type = element.children(":selected").attr("data-type");
            statueMap.renderStatues(["/api/statues/", type, "/", $(ui.item).val()].join(""));
          break;

          default:
            break;
        }
        return true;
      } 

 
      $("dl#categories-list").delegate("a.category", "click", function(event) {
        event.preventDefault();
        var element = $(this);

        element.closest("dl").find("dt[class=active]").removeClass();
        element.closest("dt").addClass("active");

        if (element.attr("data-id") !== undefined) {
          Fragmentized.StatueMap.renderStatues(["/api/statues/category/", element.attr("data-id")].join(""));
        }
      });
      
    },

    toggleDataDefinition: function() {
      $("dl#related-work, dl#categories-list").find("dd").hide();
      $("dl#related-work, dl#categories-list").delegate("a.more", "click", function(event) {
        event.preventDefault();
        var element = $(this);
        var dataTerm = element.closest("dt");
        var dataDefinition = dataTerm.next("dd");
        var parent = dataTerm.parent();

        if (dataDefinition.text() !== "")
          dataDefinition.slideToggle();

      });
    },

    renderStatues: function(url) {
      $.getJSON(url, StatueMap.incomingDataHandler); 
    },

    parseStatue: function(statue) {
      var position = StatueMap.map.createPosition(statue.latitude, statue.longitude);
      //StatueMap.map.createMarkerFrom(position);
      //marker.setTitle(statue.title);
      var marker = new Fragmentized.ImageMarker({
        map: StatueMap.map.getMap(),
        position: position,
        image: statue.asset,
        title: statue.title
      });
      
      StatueMap.map.addMarker(marker);
      marker.index = StatueMap.map.getMarkers().length;
      StatueMap.addMarkerToList(marker, statue);
      //StatueMap.map.setInfoWindow(marker, Formatter.getStatue(statue));
    },

    addMarkerToList: function(marker, statue) {
      var locationHolder = $("<li />").addClass("clearfix").attr("data-id", marker.index);
      locationHolder.append($("<div />").text(marker.getTitle())).appendTo(StatueMap.mapOverlay);
      locationHolder.data("info", statue);
      
      google.maps.event.addListener(marker, "click", function(event) {
        StatueMap.displayStatueInfo(statue);      
      });
    },
    
    incomingDataHandler: function(data) {
      StatueMap.map.clearOverlays();
      if (StatueMap.mapOverlay)
        StatueMap.mapOverlay.empty();
      $("#statue-preview").empty();
      if ($.isEmptyObject(data))
        return null;

      var statue = ($.isArray(data)) ? data[0] : data;

      var position = StatueMap.map.createPosition(statue.latitude, statue.longitude);
      StatueMap.map.centerMap(position);

      if ($.isArray(data)) {
        for (var i = 0, length = data.length; i < length; i++) {
          StatueMap.parseStatue(data[i]);
        }
      }
      else {
        StatueMap.parseStatue(data);
      }
      return null;
    },
    
    mapOverlayClickHandler: function(event) {
      var marker = StatueMap.map.getMarkerAt(($(this).attr("data-id")));
      StatueMap.displayStatueInfo($(this).data("info"));
      if (marker !== null) {
        StatueMap.map.getMap().panTo(marker.getPosition());
        StatueMap.map.infoWindowFor(marker);
      }
    },

    displayStatueInfo: function(statue) {
      var element = $("#statue-preview");
      if (!element.is(":hidden"))
        element.html(Formatter.getStatue(statue));
      else
        element.hide().html(Formatter.getStatue(statue)).slideDown();
    }
  };

  var Formatter = {
    getStatue: function(statue) {
      return JST["statues/infowindow"]({
        asset: statue.asset,
        statue: statue.title,
        location: statue.location,
        artist: statue.artist,
        entity: statue.entity,
        url: statue.url,
        artistUrl: statue.artist_url,
        entityUrl: statue.entity_url
      });
    },

    getGallery: function(items) {
      var fragments = [];
      for (var i = 0, length = items.length; i < length; i++) {
        var valueObject = {
          title: items[i].title,
          thumb: items[i].thumb,
          url: (items[i].embed !== null) ? ["/api/assets", items[i].id].join('/') : items[i].url
        };

        if (items[i].embed === null)
          fragments.push(JST["shared/gallery_item_image"](valueObject));
        else
          fragments.push(JST["shared/gallery_item_video"](valueObject));
      }
      return fragments.join("\n");
    }
  };

  Fragmentized.StatueMap = StatueMap;
  Fragmentized.Formatter = Formatter;
})();
