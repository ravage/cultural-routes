(function() {
  var ImageMarker = function(options) {
    this.title = "";
    this.setValues(options);
     
    var div = this._div = document.createElement("div");
    div.innerHTML = ['<img src="', this.image, '" alt="Marker Image">'].join("");
    div.className = "imagemarker-overlay";
  };

  // ImageMarker is derived from google.maps.OverlayView
  ImageMarker.prototype = new google.maps.OverlayView();

  ImageMarker.prototype.onAdd = function() {
    var pane = this.getPanes().overlayImage;
    pane.appendChild(this._div);
    
    var self = this;
    this._listeners = [
      google.maps.event.addDomListener(this._div, "click", function(event) {
        google.maps.event.trigger(self, "click");
      })
    ];
  };

  ImageMarker.prototype.onRemove = function() {
    this._div.parentNode.removeChild(this._div);
    var length = this._listeners.length;
    for (var i = 0; i < length; i++) {
      google.maps.event.removeListener(this._listeners[i]);
    }
  };

  ImageMarker.prototype.draw = function() {
    var projection = this.getProjection();
    var position = projection.fromLatLngToDivPixel(this.position);
    var div = this._div;
    var offset = {};

    if (document.defaultView) {
        offset.x = parseInt(document.defaultView.getComputedStyle(this._div, null).paddingLeft, 10);
        offset.y = parseInt(document.defaultView.getComputedStyle(this._div, null).paddingBottom, 10);
    } 
    else {
        offset.x = parseInt(this._div.currentStyle.paddingLeft, 10);
        offset.y = parseInt(this._div.currentStyle.paddingBottom, 10);
    }


    div.style.left = [position.x - Math.abs(this._div.offsetWidth / 2) + offset.x, "px"].join("");
    div.style.top = [position.y - this._div.offsetHeight + offset.y, "px"].join("");
  };

  ImageMarker.prototype.getTitle = function() {
    return this.title;
  };

  ImageMarker.prototype.setTitle = function(value) {
    this.title = value;
  };

  ImageMarker.prototype.getPosition = function() {
    return this.position;
  };

  function ListOverlay(map) {
    var controlDiv = document.createElement('DIV');
    this.div = controlDiv;
    var list = document.createElement("UL");
    list.setAttribute("id", "map-overlay");
    controlDiv.style.width = "272px";
    //controlDiv.style.margin = "0 0 0 0";
    controlDiv.appendChild(list);

    map.controls[google.maps.ControlPosition.RIGHT].push(this.div);
  }


  Fragmentized.ListOverlay = ListOverlay; 
  Fragmentized.ImageMarker = ImageMarker;
})();
