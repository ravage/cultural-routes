set :application, "routes"

role :app, "ankara.ismai.pt" 
role :web, "ankara.ismai.pt" 
role :db,  "ankara.ismai.pt", :primary => true

set :repository, "."
set :scm, :git
set :deploy_via, :copy
set :deploy_to, "/home/celc/ruby_apps/#{application}"
set :copy_cache, true 
set :copy_compression, :gzip
set :use_sudo, false
set :copy_exclude, [".git"]
ssh_options[:username] = "celc"
#set :default_environment, { 
  #'PATH' => "/home/celc/.rvm/gems/ruby-1.9.2-head/bin:/home/celc/.rvm/gems/ruby-1.9.2-head@global/bin:/home/celc/.rvm/rubies/ruby-1.9.2-head/bin:/home/celc/.rvm/bin:$PATH",
  #'RUBY_VERSION' => 'ruby-1.9.2-head',
  #'GEM_HOME' => '/home/celc/.rvm/gems/ruby-1.9.2-head',
  #'GEM_PATH' => '/home/celc/.rvm/gems/ruby-1.9.2-head:/home/celc/.rvm/gems/ruby-1.9.2-head@global',
  #'BUNDLE_PATH' => '/home/celc/.rvm/gems/ruby-1.9.2-head'
#}

after "deploy:symlink", :copy

task :bundle, :hosts => "ankara.ismai.pt"  do
  run "cd #{release_path}; bundle install --relock"
end

task :copy do
  upload("../production_cfg/database.yml", "#{current_path}/config/database.yml", :mode => 0600)
  upload("../production_cfg/application.rb", "#{current_path}/config/application.rb", :mode => 0600)
  upload("config/initializers/cookie_verification_secret.rb", "#{current_path}/config/initializers/cookie_verification_secret.rb", :mode => 0600)
end

namespace :deploy do
  task :start, :roles => [:web, :app] do
     run "thin -C /etc/thin/routes.yml start"
  end
  
  task :stop, :roles => [:web, :app] do
    run "thin -C /etc/thin/routes.yml stop"
  end
  
  task :restart, :roles => [:web, :app] do
     deploy.stop
     deploy.start
   end
end
