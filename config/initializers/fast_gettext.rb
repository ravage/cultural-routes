FastGettext.add_text_domain('cultural_routes',:path => 'locale')
FastGettext.text_domain = 'cultural_routes'
FastGettext.available_locales = ['en', 'pt'] # only allow these locales to be set (optional)
FastGettext.locale = 'pt'
include FastGettext::Translation

