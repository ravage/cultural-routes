Paperclip.interpolates :poly do |attachment, style|
  attachment.instance.assetable_type.downcase.pluralize
end
